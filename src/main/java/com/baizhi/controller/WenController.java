package com.baizhi.controller;

import com.baizhi.entity.Banner;
import com.baizhi.entity.Si;
import com.baizhi.entity.VO.WenYinVO;
import com.baizhi.entity.Wen;
import com.baizhi.entity.WenYin;
import com.baizhi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
public class WenController {
    @Autowired
    private WenService wenService;



    @RequestMapping(value = "/listen/" ,method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> showlisten(Integer page, Integer rows){
        int start = (page-1)*rows;
        List list = wenService.selectSiWenBypage(start, rows);
        long sum=wenService.getWenRowCount();
        Map<String,Object> map=new HashMap<>();
        map.put("total",sum);
        map.put("rows",list);
        return map;
    }

    @RequestMapping(value = "/listen/removeBatch")
    @ResponseBody
    public Map removeBatch(String[] wid) {
        Map<String,String> map=  new HashMap<>();
        wenService.removeBatch(wid);
        map.put("code","200");
        return map;
    }

    @RequestMapping("/listen/addlisten")
    @ResponseBody
    public Map addlisten(Wen wen, MultipartFile uploadFile) {
        String filename = uploadFile.getOriginalFilename();
        String wid = UUID.randomUUID().toString();
        Map<String, String> map = new HashMap<>();
        wen.setWenImage("audioCollection/" + filename);
        try {
            uploadFile.transferTo(new File("D:\\ideawork\\cmfzManager\\target\\TestMaven\\picture\\audioCollection" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        wen.setWenId(wid);
        wenService.insertNonEmptyWen(wen);
        map.put("200", "成功");
        return map;
    }

    @RequestMapping("/listen/selectOne")
    @ResponseBody
    public Wen selectOne(String wid){
        return wenService.selectWenById(wid);
    }


    @RequestMapping("/listen/Update")
    @ResponseBody
    public Map updatelisten(Wen wen, MultipartFile uploadFile) {

        String filename = uploadFile.getOriginalFilename();
        Map<String, String> map = new HashMap<>();
        wen.setWenImage("audioCollection/" + filename);
        try {
            uploadFile.transferTo(new File("D:\\ideawork\\cmfzManager\\target\\TestMaven\\picture\\audioCollection" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        wenService.updateNonEmptyWenById(wen);

        map.put("200", "成功");
        return map;

    }
}
