package com.baizhi.controller;

import com.baizhi.entity.*;
import com.baizhi.service.SiService;
import com.baizhi.service.SiWenService;
import com.baizhi.service.WenService;
import com.baizhi.service.WenYinService;
import com.baizhi.utils.LuceneUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
public class SiController {

    @Autowired
    private SiService siService;
    @RequestMapping("/guru/ShowSi")
    @ResponseBody
    public List ShowSi(){
        return siService.selectSi();
    }

    @RequestMapping(value = "/guru/" ,method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> showSi(Integer page, Integer rows){
        int start = (page-1)*rows;
        List list = siService.selectSiWenBypage(start, rows);
        long sum=siService.getSiRowCount();
        Map<String,Object> map=new HashMap<>();
        map.put("total",sum);
        map.put("rows",list);
        return map;
    }

    @RequestMapping(value = "/guru/removeBatch")
    @ResponseBody
    public Map removeBatch(String[] aid) {
        System.out.println(1);
        Map<String,String> map=  new HashMap<>();
        siService.removeBatch(aid);
        map.put("code","200");
        return map;
    }

    @RequestMapping("/guru/addguru")
    @ResponseBody
    public Map addguru(Si si, MultipartFile uploadFile) {
        String filename = uploadFile.getOriginalFilename();
        String siid = UUID.randomUUID().toString();
        Map<String, String> map = new HashMap<>();
        si.setSiImage("shangshi/" + filename);
        try {
            uploadFile.transferTo(new File("F:\\ApacheTomcat\\cmfz\\picture\\shangshi" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        si.setSiId(siid);
        siService.insertNonEmptySi(si);
        map.put("200", "成功");
        return map;
    }

    @RequestMapping("/guru/selectOne")
    @ResponseBody
    public Si selectOne(String sid){
        return siService.selectSiById(sid);
    }


    @RequestMapping("/guru/Update")
    @ResponseBody
    public Map updateguru(Si si, MultipartFile uploadFile) {

        String filename = uploadFile.getOriginalFilename();
        Map<String, String> map = new HashMap<>();
        si.setSiImage("shangshi/" + filename);
        try {
            uploadFile.transferTo(new File("F:\\ApacheTomcat\\cmfz\\picture\\shangshi" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        siService.updateNonEmptySiById(si);

        map.put("200", "成功");
        return map;

    }
}
