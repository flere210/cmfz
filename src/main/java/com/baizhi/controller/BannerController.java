package com.baizhi.controller;

import com.baizhi.entity.Banner;
import com.baizhi.entity.SiWen;
import com.baizhi.service.BannerService;
import com.baizhi.service.ResourceService;
import com.baizhi.utils.LuceneUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.stream.XMLOutputFactory;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
public class BannerController {
    @Autowired
    private BannerService bannerService;
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/banner", method = RequestMethod.GET)
    @ResponseBody
    public Map selectMenu(Integer page,Integer rows) {
        int start = (page-1)*rows;
        Map<String,Object> map=new HashMap<>();
        List<Banner> list = null;
        list = (List<Banner>) redisTemplate.opsForValue().get("bannerlist-"+start+rows);
        if (list != null) {
           map.put("rows",list);
        } else {
            list = bannerService.selectBannerByPage(start,rows);
            redisTemplate.opsForValue().set("bannerlist-"+start+rows, list);
            map.put("rows",list);
        }
        long sum=bannerService.getBannerRowCount();
        map.put("total",sum);
        return map;
    }
    @RequestMapping("/banner/selectOne")
    @ResponseBody
    public Banner selectOne(String bid){
        System.out.println(bannerService.selectBannerById(Integer.parseInt(bid)));

        return bannerService.selectBannerById(Integer.parseInt(bid));
    }
    @RequestMapping(value = "/banner/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List selectOneMenu() {
        return bannerService.selectBanner();
    }

    @RequestMapping(value = "/banner", method = RequestMethod.POST)
    @ResponseBody
    public Map addBanner(Banner b, MultipartFile uploadFile) {
        String filename = uploadFile.getOriginalFilename();
        Map<String, String> map = new HashMap<>();
        b.setBannerImage("shouye/" + filename);
        try {
            uploadFile.transferTo(new File("F:\\ApacheTomcat\\cmfz\\picture\\shouye" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        //b.setBannerId(26);
        bannerService.insertBanner(b);
        map.put("200", "成功");
        Set keys = redisTemplate.keys("bannerlist-*");
        redisTemplate.delete(keys);
        map.put("code", "200");
        return map;

    }

    @RequestMapping("/banner/Update")
    @ResponseBody
    public Map updateBanner( Banner banner, MultipartFile uploadFile) {

        String filename = uploadFile.getOriginalFilename();
        Map<String, String> map = new HashMap<>();
        banner.setBannerImage("shouye/" + filename);

        try {
            uploadFile.transferTo(new File("D:\\ideawork\\cmfzManager\\target\\TestMaven\\picture\\shouye" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        bannerService.updateNonEmptyBannerById(banner);
        Set keys = redisTemplate.keys("bannerlist-*");
        redisTemplate.delete(keys);
        map.put("200", "成功");
        return map;

    }
    @RequestMapping(value = "/banner", method = RequestMethod.DELETE)
    @ResponseBody
    public Map removeBatch(String aid) {
        Map<String, String> map = new HashMap<>();

        String[] split = aid.split("-");

        int[] ids = new int[split.length];
        for (int i = 0; i < ids.length; i++) {
            ids[i] = Integer.parseInt(split[i]);
        }
        for (int i = 0; i < ids.length; i++) {
            System.out.println(ids[i]);
        }
        bannerService.removeBatch(ids);

        Set keys = redisTemplate.keys("bannerlist-*");
        redisTemplate.delete(keys);
        map.put("code", "200");
        return map;
    }

}
