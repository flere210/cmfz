package com.baizhi.controller;

import com.baizhi.entity.User;
import com.baizhi.entity.Xiu;
import com.baizhi.service.UserService;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Controller
public class UserPoiController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/userpoi/",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> showUser(Integer page, Integer rows){
        int start = (page-1)*rows;
        List list = userService.selectUserBypage(start, rows);
        long sum=userService.getUserRowCount();
        Map<String,Object> map=new HashMap<>();
        map.put("total",sum);
        map.put("rows",list);
        return map;
    }
    @RequestMapping(value = "/userpoi/removeBatch")
    @ResponseBody
    public Map removeBatch(String[] xid) {
        Map<String,String> map=  new HashMap<>();
        userService.removeBatch(xid);
        map.put("code","200");
        return map;
    }

    @RequestMapping("/userpoi/addguru")
    @ResponseBody
    public Map addguru(User user) {
        Map<String, String> map = new HashMap<>();
        userService.insertNonEmptyUser(user);
        map.put("200", "成功");
        return map;
    }

    @RequestMapping("/userpoi/selectOne")
    @ResponseBody
    public User selectOne(Integer uid){
        return userService.selectUserById(uid);
    }


    @RequestMapping("/userpoi/Update")
    @ResponseBody
    public Map updateguru(User user) {

        Map<String, String> map = new HashMap<>();
        userService.updateNonEmptyUserById(user);

        map.put("200", "成功");
        return map;

    }
    //导出数据Excel
    @RequestMapping("/userpoi/exportUser")
    public void exportUser(Integer[] id, HttpServletResponse res){
        //设置响应体内容
        res.setContentType("application/octet-stream");
        res.setHeader("content-Disposition","attachament;filename=userInfor.xls");
        //编写POI代码
        HSSFWorkbook workbook=new HSSFWorkbook();
        //创建工作薄
        HSSFSheet sheet=workbook.createSheet("user");
        HSSFRow row=sheet.createRow(0);
        row.createCell(0).setCellValue("姓名");
        row.createCell(1).setCellValue("性别");
        row.createCell(2).setCellValue("电话");
        row.createCell(3).setCellValue("昵称");
        row.createCell(4).setCellValue("签名");
        row.createCell(5).setCellValue("用户所在省");
        row.createCell(6).setCellValue("用户所在市");
        List<User> userList=new ArrayList<>();
        for(Integer i:id){
            userList.add(userService.selectUserById(i));
        }
        int i=1;
        for(User u:userList){
            System.out.println(u);
            HSSFRow rowi = sheet.createRow(i);
            rowi.createCell(0).setCellValue(u.getName());
            rowi.createCell(1).setCellValue(u.getSex());
            rowi.createCell(2).setCellValue(u.getTelphone());
            rowi.createCell(3).setCellValue(u.getNickname());
            rowi.createCell(4).setCellValue(u.getAutograph());
            rowi.createCell(5).setCellValue(u.getUserSheng());
            rowi.createCell(6).setCellValue(u.getUserShi());
            i++;
        }
        ServletOutputStream os = null;
        try {
            os = res.getOutputStream();
            workbook.write(os);

        } catch (IOException e) {
            e.printStackTrace();

        }finally{
            if(os!=null){
                try {
                    os.flush();
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    //导入数据
    @RequestMapping("/userpoi/importPoi")
    @ResponseBody
    public Map importPoi(MultipartFile file)  {
        HashMap map=new HashMap();
        //获取文件流数据
        try {
            InputStream inputStream = file.getInputStream();
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheetAt = workbook.getSheetAt(0);
            //获取所有行
            int hang=sheetAt.getPhysicalNumberOfRows();
            System.out.println(hang+"-------读取的行数-------");
            for(int i=1;i<hang;i++){
                String name= sheetAt.getRow(i).getCell(0).getStringCellValue();
                String sex= sheetAt.getRow(i).getCell(1).getStringCellValue();
                String telphone= sheetAt.getRow(i).getCell(2).getStringCellValue();
                String nickname= sheetAt.getRow(i).getCell(3).getStringCellValue();
                String autograph= sheetAt.getRow(i).getCell(4).getStringCellValue();
                String userSheng= sheetAt.getRow(i).getCell(5).getStringCellValue();
                String userShi= sheetAt.getRow(i).getCell(6).getStringCellValue();
                //直接添加
                User u=new User();
                u.setName(name);
                u.setSex(sex);
                u.setTelphone(telphone);
                u.setNickname(nickname);
                u.setAutograph(autograph);
                u.setUserSheng(userSheng);
                u.setUserShi(userShi);
                userService.insertNonEmptyUser(u);
            }
            map.put("code","200");
            map.put("msg","导入成功");
        } catch (IOException e) {
            e.printStackTrace();
            map.put("code","500");
            map.put("msg","导入失败");
        }
        return map;
    }

}
