package com.baizhi.controller;

import com.baizhi.entity.Banner;
import com.baizhi.entity.Log;
import com.baizhi.service.BannerService;
import com.baizhi.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class LogController {
    @Autowired
    private LogService logService;

    @RequestMapping(value = "/Log", method = RequestMethod.GET)
    @ResponseBody
    public Map selectLog(Integer page, Integer rows) {
        System.out.println(page+""+rows);
        Map<String, Object> map = new HashMap<>();
        int start = (page - 1) * rows;
        List<Log> list = logService.selectLogBypage(start,rows);
        System.out.println(list);
        map.put("rows", list);
        long sum = logService.getLogRowCount();
        map.put("total", sum);
        return map;
    }


}
