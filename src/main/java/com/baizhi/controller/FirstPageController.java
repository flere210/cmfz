package com.baizhi.controller;

import com.baizhi.entity.Banner;
import com.baizhi.entity.Si;
import com.baizhi.entity.Wen;
import com.baizhi.service.BannerService;
import com.baizhi.service.SiService;
import com.baizhi.service.WenService;
import com.baizhi.service.XiuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/firstPage")
public class FirstPageController {
    @Autowired
    private BannerService bannerService;
    @Autowired
    private SiService siService;
    @Autowired
    private WenService wenService;
    @Autowired
    private XiuService xiuService;

    @RequestMapping("/showFirstPage")
    @ResponseBody

    public Map showFirstPage(){
        HashMap<String, List> map = new HashMap<>();
        List<Banner> banners = bannerService.selectBanner();
        List<Wen> wens = wenService.selectWen();
        List<Si> sis = siService.selectSi();
        map.put("header",banners);
        map.put("listens",wens);
        map.put("gurn",sis);
        return map;
    }
    @RequestMapping("/showFirstPage/{Type}")
    @ResponseBody
    public List showWenPage(@PathVariable("Type") String type){
        if ("si".equals(type)){
            return siService.selectSi();
        }else if ("wen".equals(type)){
            return wenService.selectWen();
        }
        return xiuService.selectXiu();

    }
}
