package com.baizhi.controller;

import com.baizhi.entity.Si;
import com.baizhi.entity.Xiu;
import com.baizhi.entity.XiuCount;
import com.baizhi.service.XiuCountService;
import com.baizhi.service.XiuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Controller
public class XiuController {

    @Autowired
    private XiuService xiuService;


    @RequestMapping(value = "/category/",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> showXiu(Integer page, Integer rows){
        int start = (page-1)*rows;
        List list = xiuService.selectXiuBypage(start, rows);
        long sum=xiuService.getXiuRowCount();
        Map<String,Object> map=new HashMap<>();
        map.put("total",sum);
        map.put("rows",list);
        return map;
    }
    @RequestMapping(value = "/category/removeBatch")
    @ResponseBody
    public Map removeBatch(String[] xid) {
        System.out.println(1);
        Map<String,String> map=  new HashMap<>();
        xiuService.removeBatch(xid);
        map.put("code","200");
        return map;
    }

    @RequestMapping("/category/addguru")
    @ResponseBody
    public Map addguru(Xiu xiu) {
        String siid = UUID.randomUUID().toString();
        Map<String, String> map = new HashMap<>();
        xiu.setXiuId(siid);
        xiuService.insertXiu(xiu);
        map.put("200", "成功");
        return map;
    }

    @RequestMapping("/category/selectOne")
    @ResponseBody
    public Xiu selectOne(String sid){
        return xiuService.selectXiuById(sid);
    }


    @RequestMapping("/category/Update")
    @ResponseBody
    public Map updateguru(Xiu xiu) {

        Map<String, String> map = new HashMap<>();
        xiuService.updateNonEmptyXiuById(xiu);

        map.put("200", "成功");
        return map;

    }
}




