package com.baizhi.controller;

import com.baizhi.entity.VO.ResourceVO;
import com.baizhi.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MenuController {
    @Autowired
    private ResourceService resourceService;

    /*@RequestMapping(value = "/menu",method = RequestMethod.GET)
    @ResponseBody
    public List selectMenu(){
        return resourceService.selectMenu();
    }*/

    @RequestMapping(value = "/menu",method = RequestMethod.GET)
    @ResponseBody
    public List selectMenuByname(){

        return resourceService.selectMenu();
    }

    @RequestMapping(value = "/menu/{id}",method = RequestMethod.GET)
    @ResponseBody
    public List selectOneMenu(){
        return resourceService.selectMenu();
    }

}
