package com.baizhi.controller;

import com.baizhi.entity.Wen;
import com.baizhi.entity.WenYin;
import com.baizhi.service.WenService;
import com.baizhi.service.WenYinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class WenYinController {
    @Autowired
    private WenYinService wenYinService;



    @RequestMapping(value = "/WenYin/" ,method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> showlisten(Integer page, Integer rows){
        int start = (page-1)*rows;
        List list = wenYinService.selectSiWenBypage(start, rows);
        long sum=wenYinService.getWenYinRowCount();
        Map<String,Object> map=new HashMap<>();
        map.put("total",sum);
        map.put("rows",list);
        return map;
    }

    @RequestMapping(value = "/WenYin/removeBatch")
    @ResponseBody
    public Map removeBatch(String[] yid) {
        Map<String,String> map=  new HashMap<>();
        System.out.println(yid[0]);
        wenYinService.removeBatch(yid);
        map.put("code","200");
        return map;
    }

    @RequestMapping("/WenYin/addlisten")
    @ResponseBody
    public Map addlisten(WenYin wenYin, MultipartFile uploadFile) {
        String filename = uploadFile.getOriginalFilename();
        String yid = UUID.randomUUID().toString();
        Map<String, String> map = new HashMap<>();
        wenYin.setYinUrl("yin/" + filename);
        try {
            uploadFile.transferTo(new File("D:\\ideawork\\cmfzManager\\target\\TestMaven\\picture\\yin" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        wenYin.setYinId(yid);
        wenYinService.insertNonEmptyWenYin(wenYin);
        map.put("200", "成功");
        return map;
    }

    @RequestMapping("/WenYin/selectOne")
    @ResponseBody
    public WenYin selectOne(String yid){
        return wenYinService.selectWenYinById(yid);
    }


    @RequestMapping("/WenYin/Update")
    @ResponseBody
    public Map updatelisten(WenYin wenYin, MultipartFile uploadFile) {

        String filename = uploadFile.getOriginalFilename();
        Map<String, String> map = new HashMap<>();
        wenYin.setYinUrl("yin/" + filename);
        try {
            uploadFile.transferTo(new File("D:\\ideawork\\cmfzManager\\target\\TestMaven\\picture\\yin" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        System.out.println(wenYin);
        wenYinService.updateNonEmptyWenYinById(wenYin);

        map.put("200", "成功");
        return map;

    }
}
