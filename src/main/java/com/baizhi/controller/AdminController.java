package com.baizhi.controller;

import com.baizhi.entity.Admin;
import com.baizhi.entity.AmdinRole;
import com.baizhi.entity.Role;
import com.baizhi.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admins")
public class AdminController {
    @Autowired
    private AdminService adminsService;

    @RequestMapping("/login")
    @ResponseBody
    public Map<String,String> login (String username, String password, HttpSession session){
        /*String s = adminsService.selectByName(username, password);
        if(s.equals("ok")){
            List<AdminsRole> adminsRoleList = adminsService.selectAdminsRole(username);
            Admins admins = new Admins();
            admins.setUsername(username);
            Integer[] integers = new Integer[adminsRoleList.size()];
            for(int i=0;i<adminsRoleList.size();i++){
                integers[i] = adminsRoleList.get(i).getRoleid();
            }
            admins.setRoleid(integers);
            //System.out.println(admins);
            session.setAttribute("admins",admins);
        }
        Map<String,String> map = new HashMap<>();
        map.put("url","/jsp/main.jsp");*/
        return null;
    }

    @RequestMapping("/queryAllAdmins")
    @ResponseBody
    public List<Admin> queryAllAdmins(){
        List<Admin> adminsList = adminsService.selectAdmin();

        for (Admin a:adminsList){
            List<Role> roleList = adminsService.selectRolesByAdminsName(a.getUsername());
            a.setRoleList(roleList);
        }

        return adminsList;
    }

    @RequestMapping("/updateAdmins")
    @ResponseBody
    public Map<String,String> updateAdmins(String id,String username,String beforepassword,Integer[] ids){
        Admin admin = new Admin();
        admin.setId(id);
        admin.setUsername(username);
        admin.setBeforepassword(beforepassword);
        adminsService.updateNonEmptyAdminById(admin);
        adminsService.deleteRoleByadminName(username);
        AmdinRole adminRole = new AmdinRole();
        adminRole.setUsername(username);
        for(Integer i:ids){
            adminRole.setRoleid(i);
            adminsService.insertAdminsRole(adminRole);
        }
        Map<String,String> map = new HashMap<>();
        map.put("msg","修改成功");
        map.put("code","200");
        return map;
    }
}
