package com.baizhi.controller;

import com.baizhi.entity.Banner;
import com.baizhi.entity.Si;
import com.baizhi.entity.SiWen;
import com.baizhi.service.SiService;
import com.baizhi.service.SiWenService;
import com.baizhi.utils.LuceneUtil;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.processing.RoundEnvironment;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
public class SiWenController {
    @Autowired
    private SiWenService siWenService;

    @RequestMapping(value = "/SiWen",method = RequestMethod.GET)
    @ResponseBody
    public Map showPage(Integer page,Integer rows){
        int start = (page-1)*rows;
        List list = siWenService.selectSiWenBypage(start, rows);
        long sum=siWenService.getSiWenRowCount();
        Map<String,Object> map=new HashMap<>();
        map.put("total",sum);
        map.put("rows",list);
        return map;
    }

    @RequestMapping(value = "/SiWen/removeBatch")
    @ResponseBody
    public Map removeBatch( String[] aid) {
        Map<String,String> map=  new HashMap<>();

        siWenService.removeBatch(aid);
        try {
            LuceneUtil.deleteIndexDB(aid);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            LuceneUtil.closeAndCommit();
        }
        map.put("code","200");
        return map;
    }

    @RequestMapping("/SiWen/selectOne")
    @ResponseBody
    public SiWen selectOne(String wzid){
        return siWenService.selectSiWenById(wzid);

    }

    @RequestMapping(value = "/SiWen/removeIndex")
    @ResponseBody
    public Map removeIndex(){
        try {
            LuceneUtil.deleteIndexDB();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            LuceneUtil.closeAndCommit();
        }
        Map<String,String> map=  new HashMap<>();
        map.put("code","200");
        return map;
    }


    @RequestMapping(value = "/SiWen/Add")
    @ResponseBody
    public Map addBanner(String siName,SiWen siWen, MultipartFile uploadFile) {
        String filename = uploadFile.getOriginalFilename();
        String wzid = UUID.randomUUID().toString();
        Map<String, String> map = new HashMap<>();
        siWen.setWzImage("si/" + filename);
        try {
            uploadFile.transferTo(new File("D:\\ideawork\\cmfzManager\\target\\TestMaven\\picture\\si" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        siWen.setWzId(wzid);
        siWen.setWzDate(new Date());
        siWen.setWzCount(0);
        siWenService.insertSiWen(siWen);
        siWen.setSiId(siName);
        System.out.println(siWen);
        try {
            LuceneUtil.createIndexDB(siWen);
        } catch (Exception e) {

        }finally {
            LuceneUtil.closeAndCommit();
        }
        map.put("200", "成功");
        return map;

    }

    @RequestMapping(value = "/SiWen/Update")
    @ResponseBody
    public Map UpdateBanner(String siName,SiWen siWen, MultipartFile uploadFile) {

        String filename = uploadFile.getOriginalFilename();
        Map<String, String> map = new HashMap<>();
        siWen.setWzImage("si/" + filename);
        System.out.println(siWen.getWzImage()+"-----------");
        try {
            uploadFile.transferTo(new File("D:\\ideawork\\cmfzManager\\target\\TestMaven\\picture\\si" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        siWen.setWzDate(new Date());
        siWen.setWzCount(0);
        siWenService.updateSiWenById(siWen);
        siWen.setSiId(siName);
        try {
            LuceneUtil.updateIndexDB(siWen);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            LuceneUtil.closeAndCommit();
        }
        map.put("200", "成功");
        return map;

    }

    @RequestMapping(value = "/SiWen/{keyword}")
    @ResponseBody
    public Map showkeywordPage(@PathVariable("keyword") String keyword,Integer page,Integer rows) throws Exception {
        return LuceneUtil.findIndexDB(keyword,page,rows);
    }



    @RequestMapping(value = "/SiWen" ,method = RequestMethod.POST)
    @ResponseBody
    public Map createIndexDB()  {
        List<SiWen> list = siWenService.selectSiWen();
        Map<String,String> map=new HashMap<>();
        for (SiWen siWen : list) {
            System.out.println(siWen);
            try {
                LuceneUtil.createIndexDB(siWen);
                map.put("code","200");
            } catch (Exception e) {
                map.put("code","500");
                e.printStackTrace();
            }finally {
                LuceneUtil.closeAndCommit();
            }
        }
        return map;


    }


}
