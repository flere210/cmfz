package com.baizhi.controller;

import com.baizhi.dao.TestRoleDao;
import com.baizhi.entity.Resource;
import com.baizhi.entity.Role;
import com.baizhi.entity.RoleResource;
import com.baizhi.entity.Si;
import com.baizhi.entity.VO.RoleVO;
import com.baizhi.service.ResourceService;
import com.baizhi.service.RoleResourceService;
import com.baizhi.service.RoleService;
import com.baizhi.service.SiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class ResourceController {


    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleResourceService roleResourceService;
    @Autowired
    private TestRoleDao dao;

    @RequestMapping(value = "/Resource/role")
    @ResponseBody
    public List showResource() {
        List<Role> list = roleService.selectRole();
        for (Role i : list) {
            String roleName = i.getRolename();
            // 顶级资源
            List<Resource> resources = dao.selectParentResourceByRoleName(roleName);

            for (Resource r : resources) {
                getSonResource(r, dao, roleName);
            }
            i.setList(resources);
        }
        return list;
    }

    // 递归查询子级资源
    public void getSonResource(Resource parentR, TestRoleDao testRoleDao, String roleName) {
        // 根据父类id查询子类
        List<Resource> sonList = testRoleDao.selectSonResource(parentR.getId(), roleName);
        // 判断当前节点集合是否为null,如果不为null 继续查询
        if (sonList != null || sonList.size() > 0) {
            // 将子级节点集合放入父级集合属性中
            parentR.setChildren(sonList);
            for (Resource r : sonList) {
                getSonResource(r, testRoleDao, roleName);
            }
        }
    }

    @RequestMapping(value = "/Resource/tree")
    @ResponseBody
    public List showtree() {
        return roleResourceService.selectSiWenBypage(1, 5);
    }

    @RequestMapping(value = "/Resource/addRole")
    @ResponseBody
    public void addRole(String[] ids, String roleName) {
        roleService.inserBatch(ids, roleName);
        Role role = new Role();
        role.setRolename(roleName);
        roleService.insertNonEmptyRole(role);

    }

    @RequestMapping(value = "/Resource/updateRole")
    @ResponseBody
    public void updateRole(String updateids, String updateid, String oldRoleName, String roleName) {

        roleService.deleteRoleByName(oldRoleName);
        roleResourceService.deleteRoleResourceByName(oldRoleName);

        String[] ids = updateids.split(",");

        roleService.inserBatch(ids, roleName);
        Role role = new Role();
        role.setRolename(roleName);
        roleService.insertNonEmptyRole(role);
    }

    @RequestMapping("/role/queryAllRoleForAdmins")
    @ResponseBody
    public List<RoleVO> queryAllRoleForAdmins(){
        List<RoleVO> roleList = roleService.selectAllRoleVO();
        return roleList;
    }

}


/*
    @RequestMapping(value = "/guru/removeBatch")
    @ResponseBody
    public Map removeBatch(String[] aid) {
        System.out.println(1);
        Map<String,String> map=  new HashMap<>();
        siService.removeBatch(aid);
        map.put("code","200");
        return map;
    }

    @RequestMapping("/guru/addguru")
    @ResponseBody
    public Map addguru(Si si, MultipartFile uploadFile) {
        String filename = uploadFile.getOriginalFilename();
        String siid = UUID.randomUUID().toString();
        Map<String, String> map = new HashMap<>();
        si.setSiImage("shangshi/" + filename);
        try {
            uploadFile.transferTo(new File("F:\\ApacheTomcat\\cmfz\\picture\\shangshi" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        si.setSiId(siid);
        siService.insertNonEmptySi(si);
        map.put("200", "成功");
        return map;
    }

    @RequestMapping("/guru/selectOne")
    @ResponseBody
    public Si selectOne(String sid){
        return siService.selectSiById(sid);
    }


    @RequestMapping("/guru/Update")
    @ResponseBody
    public Map updateguru(Si si, MultipartFile uploadFile) {

        String filename = uploadFile.getOriginalFilename();
        Map<String, String> map = new HashMap<>();
        si.setSiImage("shangshi/" + filename);
        try {
            uploadFile.transferTo(new File("F:\\ApacheTomcat\\cmfz\\picture\\shangshi" + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            map.put("500", "失败");
        }
        siService.updateNonEmptySiById(si);

        map.put("200", "成功");
        return map;

    }*/

