package com.baizhi.aop;

import com.baizhi.dao.LogDao;
import com.baizhi.entity.Log;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.condition.RequestConditionHolder;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * author: pengSir
 * CreateDate: 2018-08-09
 * TIME: 11:06
 * AIM : 日志切面
 */
public class MyAop implements MethodInterceptor {
  @Autowired private LogDao logDao;
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        // 当前执行的方法
        Method method = methodInvocation.getMethod();
        // selectUserByUserNameAndPasswrod  使用方法名 可读性差  注解  自定义注解
        String methodName = method.getName();
        // 获取当前方法注解对象
        ServiceLog annotation = method.getAnnotation(ServiceLog.class);
        String value = annotation.value();
        System.out.println(value);
        // 获取作用域
      //  HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        // 1. 谁   session
        // 2. 当前时间  new Date
        // 3. 当前执行的方法   methodName
        // 异常处理  结果

        // Object[] arguments = methodInvocation.getArguments();  获取方法的形参
        Log log = new Log();
        log.setMethodName(value);
        log.setCreateDate(new Date());
        Object proceed = null;
        // 起始时间
        try {
            // 执行当前业务层方法
            proceed = methodInvocation.proceed();
            // 结束时间   以毫秒为单位
            log.setResult("成功");
            logDao.insertLog(log);
        } catch (Exception throwable) {
            // 当程序发生异常时,需要记录异常信息 方便维护
            throwable.printStackTrace();
            System.out.println("1111111111111111");
            log.setResult(throwable.getMessage());
            logDao.insertLog(log);

        }
        // 4. 执行结果  result  proceed  当前方法的返回值
        // 5. 将日志放入数据库 进行维护
        return proceed;
    }
}
