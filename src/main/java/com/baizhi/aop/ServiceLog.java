package com.baizhi.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * author: pengSir
 * CreateDate: 2018-08-09
 * TIME: 11:30
 * AIM : 业务层 日志收集
 */
// 设置注解的使用范围
@Target(ElementType.METHOD)
// 在jvm运行时,保留注解数据
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceLog {
    // 注解属性  默认值为空串
    String value() default "";
}
