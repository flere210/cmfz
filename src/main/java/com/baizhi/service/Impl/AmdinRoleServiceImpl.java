package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.AmdinRoleDao;
import com.baizhi.entity.AmdinRole;
import com.baizhi.service.AmdinRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class AmdinRoleServiceImpl implements AmdinRoleService {
    @Autowired
    private AmdinRoleDao amdinRoleDao;
    @Override
    public long getAmdinRoleRowCount(){
        return amdinRoleDao.getAmdinRoleRowCount();
    }
    @Override
    public List<AmdinRole> selectAmdinRole(){
        return amdinRoleDao.selectAmdinRole();
    }
    @Override
    public AmdinRole selectAmdinRoleByObj(AmdinRole obj){
        return amdinRoleDao.selectAmdinRoleByObj(obj);
    }
    @Override
    public AmdinRole selectAmdinRoleById(Integer id){
        return amdinRoleDao.selectAmdinRoleById(id);
    }
    @Override
    public int insertAmdinRole(AmdinRole value){
        return amdinRoleDao.insertAmdinRole(value);
    }
    @Override
    public int insertNonEmptyAmdinRole(AmdinRole value){
        return amdinRoleDao.insertNonEmptyAmdinRole(value);
    }
    @Override
    public int deleteAmdinRoleById(Integer id){
        return amdinRoleDao.deleteAmdinRoleById(id);
    }
    @Override
    public int updateAmdinRoleById(AmdinRole enti){
        return amdinRoleDao.updateAmdinRoleById(enti);
    }
    @Override
    public int updateNonEmptyAmdinRoleById(AmdinRole enti){
        return amdinRoleDao.updateNonEmptyAmdinRoleById(enti);
    }

    public AmdinRoleDao getAmdinRoleDao() {
        return this.amdinRoleDao;
    }

    public void setAmdinRoleDao(AmdinRoleDao amdinRoleDao) {
        this.amdinRoleDao = amdinRoleDao;
    }

}