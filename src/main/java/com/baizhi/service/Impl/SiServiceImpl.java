package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.SiDao;
import com.baizhi.entity.Si;
import com.baizhi.service.SiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class SiServiceImpl implements SiService {
    @Autowired
    private SiDao siDao;
    @Override
    public long getSiRowCount(){
        return siDao.getSiRowCount();
    }
    @Override
    public List<Si> selectSi(){
        return siDao.selectSi();
    }
    @Override
    public Si selectSiByObj(Si obj){
        return siDao.selectSiByObj(obj);
    }
    @Override
    public Si selectSiById(String id){
        return siDao.selectSiById(id);
    }
    @Override
    public int insertSi(Si value){
        return siDao.insertSi(value);
    }
    @Override
    public int insertNonEmptySi(Si value){
        return siDao.insertNonEmptySi(value);
    }
    @Override
    public int deleteSiById(String id){
        return siDao.deleteSiById(id);
    }
    @Override
    public int updateSiById(Si enti){
        return siDao.updateSiById(enti);
    }
    @Override
    public int updateNonEmptySiById(Si enti){
        return siDao.updateNonEmptySiById(enti);
    }

    @Override
    public List selectSiWenBypage(int start, Integer rows) {
        return siDao.selectSiWenBypage(start,rows);
    }

    @Override
    public void removeBatch(String[] split) {
        siDao.removeBatch(split);
    }

    public SiDao getSiDao() {
        return this.siDao;
    }

    public void setSiDao(SiDao siDao) {
        this.siDao = siDao;
    }

}