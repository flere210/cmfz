package com.baizhi.service.Impl;
import java.util.List;

import com.baizhi.aop.ServiceLog;
import com.baizhi.dao.BannerDao;
import com.baizhi.entity.Banner;
import com.baizhi.service.BannerService;
import com.baizhi.service.tx.BannerServiceTX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class BannerServiceImpl implements BannerService {
    @Autowired
    private BannerDao bannerDao;
    @Autowired
    private BannerServiceTX bannerServiceTX;

    @Override
    public long getBannerRowCount(){
        return bannerDao.getBannerRowCount();
    }
    @Override
    public List<Banner> selectBanner(){
        return bannerDao.selectBanner();
    }
    @Override
    public Banner selectBannerByObj(Banner obj){
        return bannerDao.selectBannerByObj(obj);
    }
    @Override
    public Banner selectBannerById(Integer id){
        return bannerDao.selectBannerById(id);
    }

    @ServiceLog("xxxxxx")
    @Override
    public int insertBanner(Banner value){

        int  i= 0;
        try {
            i = bannerServiceTX.insertBanner(value);
            throw new RuntimeException("失败");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


    }
    @Override
    public int insertNonEmptyBanner(Banner value){
        return bannerDao.insertNonEmptyBanner(value);
    }
    @Override
    public int deleteBannerById(Integer id){
        return bannerDao.deleteBannerById(id);
    }
    @Override
    public int updateBannerById(Banner enti){
        return bannerDao.updateBannerById(enti);
    }
    @Override
    public int updateNonEmptyBannerById(Banner enti){
        return bannerDao.updateNonEmptyBannerById(enti);
    }

    @Override
    public int removeBatch(int[] aid) {
        return bannerDao.removeBatch(aid);
    }

    @Override
    public List<Banner> selectBannerByPage(Integer page, Integer rows) {

        return bannerDao.selectBannerByPage(page,rows);
    }

    public BannerDao getBannerDao() {
        return this.bannerDao;
    }

    public void setBannerDao(BannerDao bannerDao) {
        this.bannerDao = bannerDao;
    }

}