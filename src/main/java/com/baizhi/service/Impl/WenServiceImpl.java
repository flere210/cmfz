package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.WenDao;
import com.baizhi.entity.VO.WenYinVO;
import com.baizhi.entity.Wen;
import com.baizhi.service.WenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class WenServiceImpl implements WenService {
    @Autowired
    private WenDao wenDao;
    @Override
    public long getWenRowCount(){
        return wenDao.getWenRowCount();
    }
    @Override
    public List<Wen> selectWen(){
        return wenDao.selectWen();
    }
    @Override
    public Wen selectWenByObj(Wen obj){
        return wenDao.selectWenByObj(obj);
    }
    @Override
    public Wen selectWenById(String id){
        return wenDao.selectWenById(id);
    }
    @Override
    public int insertWen(Wen value){
        return wenDao.insertWen(value);
    }
    @Override
    public int insertNonEmptyWen(Wen value){
        return wenDao.insertNonEmptyWen(value);
    }
    @Override
    public int deleteWenById(String id){
        return wenDao.deleteWenById(id);
    }
    @Override
    public int updateWenById(Wen enti){
        return wenDao.updateWenById(enti);
    }
    @Override
    public int updateNonEmptyWenById(Wen enti){
        return wenDao.updateNonEmptyWenById(enti);
    }

    @Override
    public WenYinVO selectWenYin(String wid) {
        return wenDao.selectWenYin(wid);
    }

    @Override
    public List selectSiWenBypage(int start, Integer rows) {
        return wenDao.selectSiWenBypage(start,rows);
    }

    @Override
    public void removeBatch(String[] wid) {
        wenDao.removeBatch(wid);
    }

    public WenDao getWenDao() {
        return this.wenDao;
    }

    public void setWenDao(WenDao wenDao) {
        this.wenDao = wenDao;
    }

}