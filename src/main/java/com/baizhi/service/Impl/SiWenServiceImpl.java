package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.SiWenDao;
import com.baizhi.entity.SiWen;
import com.baizhi.service.SiWenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class SiWenServiceImpl implements SiWenService {
    @Autowired
    private SiWenDao siWenDao;
    @Override
    public long getSiWenRowCount(){
        return siWenDao.getSiWenRowCount();
    }
    @Override
    public List<SiWen> selectSiWen(){
        return siWenDao.selectSiWen();
    }
    @Override
    public SiWen selectSiWenByObj(SiWen obj){
        return siWenDao.selectSiWenByObj(obj);
    }
    @Override
    public SiWen selectSiWenById(String id){
        return siWenDao.selectSiWenById(id);
    }
    @Override
    public int insertSiWen(SiWen value){
        return siWenDao.insertSiWen(value);
    }
    @Override
    public int insertNonEmptySiWen(SiWen value){
        return siWenDao.insertNonEmptySiWen(value);
    }
    @Override
    public int deleteSiWenById(String id){
        return siWenDao.deleteSiWenById(id);
    }
    @Override
    public int updateSiWenById(SiWen enti){
        return siWenDao.updateSiWenById(enti);
    }
    @Override
    public int updateNonEmptySiWenById(SiWen enti){
        return siWenDao.updateNonEmptySiWenById(enti);
    }

    @Override
    public List<SiWen> selectSiWenBySid(String sid) {
        return siWenDao.selectSiWenBySid(sid);
    }

    @Override
    public int removeBatch(String[] ids) {
        return siWenDao.removeBatch(ids);
    }

    @Override
    public List selectSiWenBypage(Integer page, Integer rows) {
        return siWenDao.selectSiWenBypage(page,rows);
    }

    public SiWenDao getSiWenDao() {
        return this.siWenDao;
    }

    public void setSiWenDao(SiWenDao siWenDao) {
        this.siWenDao = siWenDao;
    }

}