package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.WenYinDao;
import com.baizhi.entity.WenYin;
import com.baizhi.service.WenYinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class WenYinServiceImpl implements WenYinService {
    @Autowired
    private WenYinDao wenYinDao;
    @Override
    public long getWenYinRowCount(){
        return wenYinDao.getWenYinRowCount();
    }
    @Override
    public List<WenYin> selectWenYin(){
        return wenYinDao.selectWenYin();
    }
    @Override
    public WenYin selectWenYinByObj(WenYin obj){
        return wenYinDao.selectWenYinByObj(obj);
    }
    @Override
    public WenYin selectWenYinById(String id){
        return wenYinDao.selectWenYinById(id);
    }
    @Override
    public int insertWenYin(WenYin value){
        return wenYinDao.insertWenYin(value);
    }
    @Override
    public int insertNonEmptyWenYin(WenYin value){
        return wenYinDao.insertNonEmptyWenYin(value);
    }
    @Override
    public int deleteWenYinById(String id){
        return wenYinDao.deleteWenYinById(id);
    }
    @Override
    public int updateWenYinById(WenYin enti){
        return wenYinDao.updateWenYinById(enti);
    }
    @Override
    public int updateNonEmptyWenYinById(WenYin enti){
        return wenYinDao.updateNonEmptyWenYinById(enti);
    }

    @Override
    public List<WenYin> selectWenYinByWid(String wid) {
        return wenYinDao.selectWenYinByWid(wid);
    }

    @Override
    public List selectSiWenBypage(int start, Integer rows) {
        return wenYinDao.selectWenYinBypage(start,rows);
    }

    @Override
    public void removeBatch(String[] wid) {
        wenYinDao.removeBatch(wid);
    }

    public WenYinDao getWenYinDao() {
        return this.wenYinDao;
    }

    public void setWenYinDao(WenYinDao wenYinDao) {
        this.wenYinDao = wenYinDao;
    }

}