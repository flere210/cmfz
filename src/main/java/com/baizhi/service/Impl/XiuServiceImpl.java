package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.XiuDao;
import com.baizhi.entity.Xiu;
import com.baizhi.service.XiuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class XiuServiceImpl implements XiuService {
    @Autowired
    private XiuDao xiuDao;
    @Override
    public long getXiuRowCount(){
        return xiuDao.getXiuRowCount();
    }
    @Override
    public List<Xiu> selectXiu(){
        return xiuDao.selectXiu();
    }
    @Override
    public Xiu selectXiuByObj(Xiu obj){
        return xiuDao.selectXiuByObj(obj);
    }
    @Override
    public Xiu selectXiuById(String id){
        return xiuDao.selectXiuById(id);
    }
    @Override
    public int insertXiu(Xiu value){
        return xiuDao.insertXiu(value);
    }
    @Override
    public int insertNonEmptyXiu(Xiu value){
        return xiuDao.insertNonEmptyXiu(value);
    }
    @Override
    public int deleteXiuById(String id){
        return xiuDao.deleteXiuById(id);
    }
    @Override
    public int updateXiuById(Xiu enti){
        return xiuDao.updateXiuById(enti);
    }
    @Override
    public int updateNonEmptyXiuById(Xiu enti){
        return xiuDao.updateNonEmptyXiuById(enti);
    }

    @Override
    public List<Xiu> selectXiuByUid(String uid) {
        return xiuDao.selectXiuByUid(uid);
    }

    @Override
    public List selectXiuBypage(int start, Integer rows) {
        return xiuDao.selectXiuBypage(start,rows);
    }

    @Override
    public void removeBatch(String[] xid) {
        xiuDao.removeBatch(xid);
    }

    public XiuDao getXiuDao() {
        return this.xiuDao;
    }

    public void setXiuDao(XiuDao xiuDao) {
        this.xiuDao = xiuDao;
    }

}