package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.RoleDao;
import com.baizhi.entity.Role;
import com.baizhi.entity.VO.RoleVO;
import com.baizhi.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;
    @Override
    public long getRoleRowCount(){
        return roleDao.getRoleRowCount();
    }
    @Override
    public List<Role> selectRole(){
        return roleDao.selectRole();
    }
    @Override
    public Role selectRoleByObj(Role obj){
        return roleDao.selectRoleByObj(obj);
    }
    @Override
    public Role selectRoleById(Integer id){
        return roleDao.selectRoleById(id);
    }
    @Override
    public int insertRole(Role value){
        return roleDao.insertRole(value);
    }
    @Override
    public int insertNonEmptyRole(Role value){
        return roleDao.insertNonEmptyRole(value);
    }
    @Override
    public int deleteRoleById(Integer id){
        return roleDao.deleteRoleById(id);
    }
    @Override
    public int updateRoleById(Role enti){
        return roleDao.updateRoleById(enti);
    }
    @Override
    public int updateNonEmptyRoleById(Role enti){
        return roleDao.updateNonEmptyRoleById(enti);
    }

    @Override
    public void inserBatch(String[] ids, String roleName) {
        roleDao.inserBatch(ids,roleName);
    }

    @Override
    public void deleteRoleByName(String oldRoleName) {
        roleDao.deleteRoleByName(oldRoleName);
    }

    @Override
    public List<RoleVO> selectAllRoleVO() {
        return roleDao.selectAllRoleVO();
    }

    @Override
    public List<Role> selectRoleByAdmin(String username) {
        return roleDao.selectRoleByAdmin(username);
    }

    public RoleDao getRoleDao() {
        return this.roleDao;
    }

    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

}