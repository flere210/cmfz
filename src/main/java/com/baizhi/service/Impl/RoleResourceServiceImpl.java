package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.RoleResourceDao;
import com.baizhi.entity.RoleResource;
import com.baizhi.service.RoleResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class RoleResourceServiceImpl implements RoleResourceService {
    @Autowired
    private RoleResourceDao roleResourceDao;
    @Override
    public long getRoleResourceRowCount(){
        return roleResourceDao.getRoleResourceRowCount();
    }
    @Override
    public List<RoleResource> selectRoleResource(){
        return roleResourceDao.selectRoleResource();
    }
    @Override
    public RoleResource selectRoleResourceByObj(RoleResource obj){
        return roleResourceDao.selectRoleResourceByObj(obj);
    }
    @Override
    public RoleResource selectRoleResourceById(Integer id){
        return roleResourceDao.selectRoleResourceById(id);
    }
    @Override
    public int insertRoleResource(RoleResource value){
        return roleResourceDao.insertRoleResource(value);
    }
    @Override
    public int insertNonEmptyRoleResource(RoleResource value){
        return roleResourceDao.insertNonEmptyRoleResource(value);
    }
    @Override
    public int deleteRoleResourceById(Integer id){
        return roleResourceDao.deleteRoleResourceById(id);
    }
    @Override
    public int updateRoleResourceById(RoleResource enti){
        return roleResourceDao.updateRoleResourceById(enti);
    }
    @Override
    public int updateNonEmptyRoleResourceById(RoleResource enti){
        return roleResourceDao.updateNonEmptyRoleResourceById(enti);
    }

    @Override
    public List selectSiWenBypage(int start, Integer rows) {
        return roleResourceDao.selectSiWenBypage(start,rows);
    }

    @Override
    public void deleteRoleResourceByName(String oldRoleName) {
        roleResourceDao.deleteRoleResourceByName(oldRoleName);
    }

    public RoleResourceDao getRoleResourceDao() {
        return this.roleResourceDao;
    }

    public void setRoleResourceDao(RoleResourceDao roleResourceDao) {
        this.roleResourceDao = roleResourceDao;
    }

}