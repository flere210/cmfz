package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.XiuCountDao;
import com.baizhi.entity.XiuCount;
import com.baizhi.service.XiuCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class XiuCountServiceImpl implements XiuCountService {
    @Autowired
    private XiuCountDao xiuCountDao;
    @Override
    public long getXiuCountRowCount(){
        return xiuCountDao.getXiuCountRowCount();
    }
    @Override
    public List<XiuCount> selectXiuCount(){
        return xiuCountDao.selectXiuCount();
    }
    @Override
    public XiuCount selectXiuCountByObj(XiuCount obj){
        return xiuCountDao.selectXiuCountByObj(obj);
    }
    @Override
    public XiuCount selectXiuCountById(String id){
        return xiuCountDao.selectXiuCountById(id);
    }
    @Override
    public int insertXiuCount(XiuCount value){
        return xiuCountDao.insertXiuCount(value);
    }
    @Override
    public int insertNonEmptyXiuCount(XiuCount value){
        return xiuCountDao.insertNonEmptyXiuCount(value);
    }
    @Override
    public int deleteXiuCountById(String id){
        return xiuCountDao.deleteXiuCountById(id);
    }
    @Override
    public int updateXiuCountById(XiuCount enti){
        return xiuCountDao.updateXiuCountById(enti);
    }
    @Override
    public int updateNonEmptyXiuCountById(XiuCount enti){
        return xiuCountDao.updateNonEmptyXiuCountById(enti);
    }

    @Override
    public List<XiuCount> selectXiuByXidUid(String xid, String uid) {
        return xiuCountDao.selectXiuByXidUid(xid,uid);
    }

    public XiuCountDao getXiuCountDao() {
        return this.xiuCountDao;
    }

    public void setXiuCountDao(XiuCountDao xiuCountDao) {
        this.xiuCountDao = xiuCountDao;
    }

}