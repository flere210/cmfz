package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.LogDao;
import com.baizhi.entity.Log;
import com.baizhi.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogDao logDao;
    @Override
    public long getLogRowCount(){
        return logDao.getLogRowCount();
    }
    @Override
    public List<Log> selectLog(){
        return logDao.selectLog();
    }
    @Override
    public Log selectLogByObj(Log obj){
        return logDao.selectLogByObj(obj);
    }
    @Override
    public Log selectLogById(Integer id){
        return logDao.selectLogById(id);
    }
    @Override
    public int insertLog(Log value){
        return logDao.insertLog(value);
    }
    @Override
    public int insertNonEmptyLog(Log value){
        return logDao.insertNonEmptyLog(value);
    }
    @Override
    public int deleteLogById(Integer id){
        return logDao.deleteLogById(id);
    }
    @Override
    public int updateLogById(Log enti){
        return logDao.updateLogById(enti);
    }
    @Override
    public int updateNonEmptyLogById(Log enti){
        return logDao.updateNonEmptyLogById(enti);
    }

    @Override
    public List<Log> selectLogBypage(int start, Integer rows) {
        return logDao.selectLogBypage(start,rows);
    }

    public LogDao getLogDao() {
        return this.logDao;
    }

    public void setLogDao(LogDao logDao) {
        this.logDao = logDao;
    }

}