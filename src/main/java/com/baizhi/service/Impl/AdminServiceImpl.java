package com.baizhi.service.Impl;
import java.util.List;
import com.baizhi.dao.AdminDao;
import com.baizhi.entity.Admin;
import com.baizhi.entity.AmdinRole;
import com.baizhi.entity.Role;
import com.baizhi.service.AdminService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminDao adminDao;
    @Override
    public long getAdminRowCount(){
        return adminDao.getAdminRowCount();
    }
    @Override
    public List<Admin> selectAdmin(){
        return adminDao.selectAdmin();
    }
    @Override
    public Admin selectAdminByObj(Admin obj){
        return adminDao.selectAdminByObj(obj);
    }
    @Override
    public Admin selectAdminById(String id){
        return adminDao.selectAdminById(id);
    }
    @Override
    public int insertAdmin(Admin value){
        return adminDao.insertAdmin(value);
    }
    @Override
    public int insertNonEmptyAdmin(Admin value){
        return adminDao.insertNonEmptyAdmin(value);
    }
    @Override
    public int deleteAdminById(String id){
        return adminDao.deleteAdminById(id);
    }
    @Override
    public int updateAdminById(Admin enti){
        return adminDao.updateAdminById(enti);
    }
    @Override
    public int updateNonEmptyAdminById(Admin enti){
        return adminDao.updateNonEmptyAdminById(enti);
    }

    public AdminDao getAdminDao() {
        return this.adminDao;
    }

    public void setAdminDao(AdminDao adminDao) {
        this.adminDao = adminDao;
    }
    @Override
    public String login(String username, String password) {
        System.out.println("-------------"+username+password);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(new UsernamePasswordToken(username,password));
            String un = subject.getPrincipal().toString();
            System.out.println("登录成功后的用户名:"+un);
        } catch (AuthenticationException e) {
            e.printStackTrace();
            Object un = subject.getPrincipal();
            System.out.println("登录失败后的用户名:"+un);
            return "error";
        }
        return  "ok";

    }

    @Override
    public List<Role> selectRolesByAdminsName(String username) {
        return adminDao.selectRolesByAdminsName(username);
    }

    @Override
    public void deleteRoleByadminName(String username) {
        adminDao.deleteRoleByadminName(username);
    }

    @Override
    public void insertAdminsRole(AmdinRole adminRole) {
        adminDao.insertAdminsRole(adminRole);
    }

}