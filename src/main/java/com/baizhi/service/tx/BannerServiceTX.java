package com.baizhi.service.tx;

import com.baizhi.aop.ServiceLog;
import com.baizhi.entity.Banner;

import java.util.List;

public interface BannerServiceTX {
	/**
	 * 获得Banner数据的总行数
	 * @return
	 */

    long getBannerRowCount();
	/**
	 * 获得Banner数据集合
	 * @return
	 */
    List<Banner> selectBanner();
	/**
	 * 获得一个Banner对象,以参数Banner对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    Banner selectBannerByObj(Banner obj);
	/**
	 * 通过Banner的id获得Banner对象
	 * @param id
	 * @return
	 */
    Banner selectBannerById(Integer id);
	/**
	 * 插入Banner到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertBanner(Banner value);
	/**
	 * 插入Banner中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptyBanner(Banner value);
	/**
	 * 通过Banner的id删除Banner
	 * @param id
	 * @return
	 */
    int deleteBannerById(Integer id);
	/**
	 * 通过Banner的id更新Banner中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateBannerById(Banner enti);
	/**
	 * 通过Banner的id更新Banner中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptyBannerById(Banner enti);

    int removeBatch(int[] aid);

    List<Banner> selectBannerByPage(Integer page, Integer rows);
}