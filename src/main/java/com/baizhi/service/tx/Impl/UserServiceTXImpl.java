package com.baizhi.service.tx.Impl;

import com.baizhi.aop.ServiceLog;
import com.baizhi.dao.UserDao;
import com.baizhi.entity.User;
import com.baizhi.service.tx.UserServiceTX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceTXImpl implements UserServiceTX {
    @Autowired
    private UserDao userDao;

    @Override
    public long getUserRowCount() {
        return 0;
    }

    @Override
    public List<User> selectUser() {
        return null;
    }

    @ServiceLog("用户登陆")
    @Override
    public User selectUserByObj(User obj) {
        User user =null;
        try{
            //user=userDao.selectUserByObj(obj);
            throw new RuntimeException("失败");
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public User selectUserById(Integer id) {
        return null;
    }

    @Override
    public int insertUser(User value) {
        return 0;
    }

    @Override
    public int insertNonEmptyUser(User value) {
        return 0;
    }

    @Override
    public int deleteUserById(Integer id) {
        return 0;
    }

    @Override
    public int updateUserById(User enti) {
        return 0;
    }

    @Override
    public int updateNonEmptyUserById(User enti) {
        return 0;
    }
}
