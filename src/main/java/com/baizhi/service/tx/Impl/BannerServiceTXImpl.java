package com.baizhi.service.tx.Impl;

import com.baizhi.aop.ServiceLog;
import com.baizhi.dao.BannerDao;
import com.baizhi.entity.Banner;
import com.baizhi.service.tx.BannerServiceTX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BannerServiceTXImpl implements BannerServiceTX {
	@Autowired
	private BannerDao bannerDao;

	@Override
	public long getBannerRowCount() {
		return 0;
	}

	@Override
	public List<Banner> selectBanner() {
		return null;
	}

	@Override
	public Banner selectBannerByObj(Banner obj) {
		return null;
	}

	@Override
	public Banner selectBannerById(Integer id) {
		return null;
	}

	@Override
	public int insertBanner(Banner value) {
		int i = 0;


			bannerDao.insertBanner(value);


		return i;
	}

	@Override
	public int insertNonEmptyBanner(Banner value) {
		return 0;
	}

	@Override
	public int deleteBannerById(Integer id) {
		return 0;
	}

	@Override
	public int updateBannerById(Banner enti) {
		return 0;
	}

	@Override
	public int updateNonEmptyBannerById(Banner enti) {
		return 0;
	}

	@Override
	public int removeBatch(int[] aid) {
		return 0;
	}

	@Override
	public List<Banner> selectBannerByPage(Integer page, Integer rows) {
		return null;
	}
}