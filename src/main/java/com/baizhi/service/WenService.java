package com.baizhi.service;
import java.util.List;

import com.baizhi.entity.VO.WenYinVO;
import com.baizhi.entity.Wen;
public interface WenService{
	/**
	 * 获得Wen数据的总行数
	 * @return
	 */
    long getWenRowCount();
	/**
	 * 获得Wen数据集合
	 * @return
	 */
    List<Wen> selectWen();
	/**
	 * 获得一个Wen对象,以参数Wen对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    Wen selectWenByObj(Wen obj);
	/**
	 * 通过Wen的id获得Wen对象
	 * @param id
	 * @return
	 */
    Wen selectWenById(String id);
	/**
	 * 插入Wen到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertWen(Wen value);
	/**
	 * 插入Wen中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptyWen(Wen value);
	/**
	 * 通过Wen的id删除Wen
	 * @param id
	 * @return
	 */
    int deleteWenById(String id);
	/**
	 * 通过Wen的id更新Wen中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateWenById(Wen enti);
	/**
	 * 通过Wen的id更新Wen中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptyWenById(Wen enti);

    WenYinVO selectWenYin(String wid);

	List selectSiWenBypage(int start, Integer rows);

	void removeBatch(String[] wid);
}