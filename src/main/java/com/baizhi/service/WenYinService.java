package com.baizhi.service;
import java.util.List;
import com.baizhi.entity.WenYin;
public interface WenYinService{
	/**
	 * 获得WenYin数据的总行数
	 * @return
	 */
    long getWenYinRowCount();
	/**
	 * 获得WenYin数据集合
	 * @return
	 */
    List<WenYin> selectWenYin();
	/**
	 * 获得一个WenYin对象,以参数WenYin对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    WenYin selectWenYinByObj(WenYin obj);
	/**
	 * 通过WenYin的id获得WenYin对象
	 * @param id
	 * @return
	 */
    WenYin selectWenYinById(String id);
	/**
	 * 插入WenYin到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertWenYin(WenYin value);
	/**
	 * 插入WenYin中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptyWenYin(WenYin value);
	/**
	 * 通过WenYin的id删除WenYin
	 * @param id
	 * @return
	 */
    int deleteWenYinById(String id);
	/**
	 * 通过WenYin的id更新WenYin中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateWenYinById(WenYin enti);
	/**
	 * 通过WenYin的id更新WenYin中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptyWenYinById(WenYin enti);

    List<WenYin> selectWenYinByWid(String wid);

    List selectSiWenBypage(int start, Integer rows);

	void removeBatch(String[] wid);
}