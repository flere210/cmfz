package com.baizhi.service;
import java.util.List;
import com.baizhi.entity.Xiu;
public interface XiuService{
	/**
	 * 获得Xiu数据的总行数
	 * @return
	 */
    long getXiuRowCount();
	/**
	 * 获得Xiu数据集合
	 * @return
	 */
    List<Xiu> selectXiu();
	/**
	 * 获得一个Xiu对象,以参数Xiu对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    Xiu selectXiuByObj(Xiu obj);
	/**
	 * 通过Xiu的id获得Xiu对象
	 * @param id
	 * @return
	 */
    Xiu selectXiuById(String id);
	/**
	 * 插入Xiu到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertXiu(Xiu value);
	/**
	 * 插入Xiu中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptyXiu(Xiu value);
	/**
	 * 通过Xiu的id删除Xiu
	 * @param id
	 * @return
	 */
    int deleteXiuById(String id);
	/**
	 * 通过Xiu的id更新Xiu中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateXiuById(Xiu enti);
	/**
	 * 通过Xiu的id更新Xiu中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptyXiuById(Xiu enti);

    List<Xiu> selectXiuByUid(String uid);

    List selectXiuBypage(int start, Integer rows);

	void removeBatch(String[] xid);
}