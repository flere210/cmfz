package com.baizhi.service;
import java.util.List;
import com.baizhi.entity.Si;
public interface SiService{
	/**
	 * 获得Si数据的总行数
	 * @return
	 */
    long getSiRowCount();
	/**
	 * 获得Si数据集合
	 * @return
	 */
    List<Si> selectSi();
	/**
	 * 获得一个Si对象,以参数Si对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    Si selectSiByObj(Si obj);
	/**
	 * 通过Si的id获得Si对象
	 * @param id
	 * @return
	 */
    Si selectSiById(String id);
	/**
	 * 插入Si到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertSi(Si value);
	/**
	 * 插入Si中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptySi(Si value);
	/**
	 * 通过Si的id删除Si
	 * @param id
	 * @return
	 */
    int deleteSiById(String id);
	/**
	 * 通过Si的id更新Si中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateSiById(Si enti);
	/**
	 * 通过Si的id更新Si中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptySiById(Si enti);

    List selectSiWenBypage(int start, Integer rows);

	void removeBatch(String[] split);
}