package com.baizhi.service;
import java.util.List;
import com.baizhi.entity.XiuCount;
public interface XiuCountService{
	/**
	 * 获得XiuCount数据的总行数
	 * @return
	 */
    long getXiuCountRowCount();
	/**
	 * 获得XiuCount数据集合
	 * @return
	 */
    List<XiuCount> selectXiuCount();
	/**
	 * 获得一个XiuCount对象,以参数XiuCount对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    XiuCount selectXiuCountByObj(XiuCount obj);
	/**
	 * 通过XiuCount的id获得XiuCount对象
	 * @param id
	 * @return
	 */
    XiuCount selectXiuCountById(String id);
	/**
	 * 插入XiuCount到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertXiuCount(XiuCount value);
	/**
	 * 插入XiuCount中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptyXiuCount(XiuCount value);
	/**
	 * 通过XiuCount的id删除XiuCount
	 * @param id
	 * @return
	 */
    int deleteXiuCountById(String id);
	/**
	 * 通过XiuCount的id更新XiuCount中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateXiuCountById(XiuCount enti);
	/**
	 * 通过XiuCount的id更新XiuCount中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptyXiuCountById(XiuCount enti);

    List<XiuCount> selectXiuByXidUid(String xid, String uid);
}