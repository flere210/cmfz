package com.baizhi.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baizhi.entity.SiWen;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.Scorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;


public class LuceneUtil {
    private static Version version;
    private static IndexWriterConfig indexConfig;
    private static Analyzer analyzer;
    private static Directory directory;
    private static ThreadLocal<IndexWriter> t = new ThreadLocal<IndexWriter>();

    static {
        try {
            directory = FSDirectory.open(new File("D://indexDB"));
            version = Version.LUCENE_44;
            analyzer = new IKAnalyzer();
            indexConfig = new IndexWriterConfig(version, analyzer);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 创建索引库
     *
     * @param siWen
     * @throws Exception
     */
    public static void createIndexDB(SiWen siWen) throws Exception {
        // 获取indexWriter 输出流
        IndexWriter indexWriter = getIndexWriter();
        // 将自定义对象转换为doc
        Document doc = getDocument(siWen);
        // 将doc 添加至 索引库
        indexWriter.addDocument(doc);

    }

    /*
     * 索引库的检索  分页
     * */
    public static Map<String, Object> findIndexDB(String keysword, Integer page, Integer rows) throws Exception {
        IndexReader ir = IndexReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(ir);
        String[] fields = {"guru", "wz_name", "wz_content"};
        QueryParser queryParser = new MultiFieldQueryParser(version, fields, analyzer);
        Query query = queryParser.parse(keysword);
        Formatter formatter = new SimpleHTMLFormatter("<font color='red'><b>", "</b></font>");
        Scorer scorer = new QueryScorer(query);

        Highlighter highlighter = new Highlighter(formatter, scorer);
        TopDocs search = indexSearcher.search(query, page * rows);

        List<SiWen> guruWenList = new ArrayList<>();
        ScoreDoc[] scoreDocs = search.scoreDocs;

        int start = (page - 1) * rows;
        int end = Math.min(scoreDocs.length, page * rows);

        for (int i = start; i < end; i++) {
            int num = scoreDocs[i].doc;
            Document doc = indexSearcher.doc(num);
            highlighter.getBestFragment(analyzer, "wz_name", doc.get("wz_name"));
            String wz_id = doc.get("wz_id");
            String wz_name = highlighter.getBestFragment(analyzer, "wz_name", doc.get("wz_name"));
            if (wz_name == null) {
                wz_name = doc.get("wz_name");
            }
            String wz_image = doc.get("wz_image");
            String wz_content = highlighter.getBestFragment(analyzer, "wz_content", doc.get("wz_content"));
            if (wz_content == null) {
                wz_content = doc.get("wz_content");
            }
            String sid = highlighter.getBestFragment(analyzer, "guru", doc.get("guru"));
            if (sid == null) {
                sid = doc.get("guru");
            }
            String wz_date = doc.get("wz_date");
            Long valueOf = Long.valueOf(wz_date);
            Date date = new Date(valueOf);
            String wz_count = doc.get("wz_count");
            SiWen guruWen = new SiWen();

            guruWen.setSiId(sid);
            guruWen.setWzContent(wz_content);
            guruWen.setWzCount(Integer.valueOf(wz_count));
            guruWen.setWzDate(date);
            guruWen.setWzId(wz_id);
            guruWen.setWzImage(wz_image);
            guruWen.setWzName(wz_name);
            guruWenList.add(guruWen);
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", guruWenList);
        map.put("total", search.totalHits);
        ir.close();
        return map;
    }

    /**
     * 索引库的更新
     *
     * @param guruWen
     * @throws Exception
     */
    public static void updateIndexDB(SiWen guruWen) throws Exception {
        IndexWriter indexWriter = getIndexWriter();
        Document doc = getDocument(guruWen);
        indexWriter.updateDocument(new Term("wz_id", guruWen.getWzId()), doc);

    }

    /**
     * 索引库的 批量删除
     *
     * @param ids
     * @throws Exception
     */
    public static void deleteIndexDB(String[] ids) throws Exception {

        IndexWriter indexWriter = getIndexWriter();

        Term[] terms = new Term[ids.length];

        for (int i = 0; i < ids.length; i++) {
            Term term = new Term("wz_id", ids[i]);
            terms[i] = term;
        }

        indexWriter.deleteDocuments(terms);

    }

    /**
     * 删除所有
     *
     * @throws Exception
     */
    public static void deleteIndexDB() throws Exception {
        IndexWriter indexWriter = getIndexWriter();
        indexWriter.deleteAll();
        indexWriter.commit();
    }

    /**
     * JavaBean 转换为 Doc
     *
     * @param siWen
     * @return
     */
    public static Document getDocument(SiWen siWen) {
        Document doc = new Document();
        doc.add(new StringField("wz_id", siWen.getWzId(), Store.YES));
        doc.add(new TextField("wz_name", siWen.getWzName(), Store.YES));
        doc.add(new StringField("wz_image", siWen.getWzImage(), Store.YES));
        doc.add(new TextField("wz_content", siWen.getWzContent(), Store.YES));
        doc.add(new TextField("guru", siWen.getSiId(), Store.YES));
        doc.add(new LongField("wz_date", siWen.getWzDate().getTime(), Store.YES));
        doc.add(new IntField("wz_count", 0, Store.YES));
        return doc;
    }

    /**
     * 获取indexWriter
     *
     * @return
     * @throws Exception
     */
    private static IndexWriter getIndexWriter() throws Exception {
        IndexWriter indexWriter = t.get();
        if (indexWriter == null) {
            indexWriter = new IndexWriter(directory, indexConfig);
            t.set(indexWriter);
        }
        return indexWriter;
    }

    /**
     * 关闭资源
     */
    public static void closeAndCommit() {
        IndexWriter indexWriter = t.get();
        if (indexWriter != null) {
            try {
                indexWriter.commit();
                indexWriter.close();
                t.remove();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Test
    public void test() {
        try {
            createIndexDB(new SiWen());
            //findIndexDB("大师",1,2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
