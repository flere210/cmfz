package com.baizhi.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Admin {
    private String id;
    private String username;
    private String password;
    private String beforepassword;
    private String passwordsalt;
    private Integer[] roleid;
    private List<Role> roleList = new ArrayList<>();

    public Admin(){}

    public Admin(String id, String username, String password, String beforepassword, String passwordsalt, Integer[] roleid, List<Role> roleList) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.beforepassword = beforepassword;
        this.passwordsalt = passwordsalt;
        this.roleid = roleid;
        this.roleList = roleList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBeforepassword() {
        return beforepassword;
    }

    public void setBeforepassword(String beforepassword) {
        this.beforepassword = beforepassword;
    }

    public String getPasswordsalt() {
        return passwordsalt;
    }

    public void setPasswordsalt(String passwordsalt) {
        this.passwordsalt = passwordsalt;
    }

    public Integer[] getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer[] roleid) {
        this.roleid = roleid;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    @Override
    public String toString() {
        return "Admins{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", beforepassword='" + beforepassword + '\'' +
                ", passwordsalt='" + passwordsalt + '\'' +
                ", roleid=" + Arrays.toString(roleid) +
                ", roleList=" + roleList +
                '}';
    }
}
