package com.baizhi.entity;
public class XiuCount {
    private String countId;
    private String countName;
    private java.util.Date countDate;
    private String xiuId;
    private Integer userId;
    private Integer countCount;
    public XiuCount() {
        super();
    }
    public XiuCount(String countId,String countName,java.util.Date countDate,String xiuId,Integer userId,Integer countCount) {
        super();
        this.countId = countId;
        this.countName = countName;
        this.countDate = countDate;
        this.xiuId = xiuId;
        this.userId = userId;
        this.countCount = countCount;
    }
    public String getCountId() {
        return this.countId;
    }

    public void setCountId(String countId) {
        this.countId = countId;
    }

    public String getCountName() {
        return this.countName;
    }

    public void setCountName(String countName) {
        this.countName = countName;
    }

    public java.util.Date getCountDate() {
        return this.countDate;
    }

    public void setCountDate(java.util.Date countDate) {
        this.countDate = countDate;
    }

    public String getXiuId() {
        return this.xiuId;
    }

    public void setXiuId(String xiuId) {
        this.xiuId = xiuId;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCountCount() {
        return this.countCount;
    }

    public void setCountCount(Integer countCount) {
        this.countCount = countCount;
    }

}
