package com.baizhi.entity;
public class Xiu {
    private String xiuId;
    private String xiuName;
    private Integer userId;
    public Xiu() {
        super();
    }
    public Xiu(String xiuId,String xiuName,Integer userId) {
        super();
        this.xiuId = xiuId;
        this.xiuName = xiuName;
        this.userId = userId;
    }
    public String getXiuId() {
        return this.xiuId;
    }

    public void setXiuId(String xiuId) {
        this.xiuId = xiuId;
    }

    public String getXiuName() {
        return this.xiuName;
    }

    public void setXiuName(String xiuName) {
        this.xiuName = xiuName;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Xiu{" +
                "xiuId='" + xiuId + '\'' +
                ", xiuName='" + xiuName + '\'' +
                ", userId=" + userId +
                '}';
    }
}
