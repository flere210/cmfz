package com.baizhi.entity;
public class WenYin {
    private String yinId;
    private String yinName;
    private String wenId;
    private String yinUrl;
    private String yinSize;
    private Integer yinCount;
    public WenYin() {
        super();
    }

    @Override
    public String toString() {
        return "WenYin{" +
                "yinId='" + yinId + '\'' +
                ", yinName='" + yinName + '\'' +
                ", wenId='" + wenId + '\'' +
                ", yinUrl='" + yinUrl + '\'' +
                ", yinSize='" + yinSize + '\'' +
                ", yinCount=" + yinCount +
                '}';
    }

    public WenYin(String yinId, String yinName, String wenId, String yinUrl, String yinSize, Integer yinCount) {
        super();
        this.yinId = yinId;
        this.yinName = yinName;
        this.wenId = wenId;
        this.yinUrl = yinUrl;
        this.yinSize = yinSize;
        this.yinCount = yinCount;
    }
    public String getYinId() {
        return this.yinId;
    }

    public void setYinId(String yinId) {
        this.yinId = yinId;
    }

    public String getYinName() {
        return this.yinName;
    }

    public void setYinName(String yinName) {
        this.yinName = yinName;
    }

    public String getWenId() {
        return this.wenId;
    }

    public void setWenId(String wenId) {
        this.wenId = wenId;
    }

    public String getYinUrl() {
        return this.yinUrl;
    }

    public void setYinUrl(String yinUrl) {
        this.yinUrl = yinUrl;
    }

    public String getYinSize() {
        return this.yinSize;
    }

    public void setYinSize(String yinSize) {
        this.yinSize = yinSize;
    }

    public Integer getYinCount() {
        return this.yinCount;
    }

    public void setYinCount(Integer yinCount) {
        this.yinCount = yinCount;
    }

}
