package com.baizhi.entity;
public class Si {
    private String siId;
    private String siName;
    private String siImage;
    private String siNickname;

    @Override
    public String toString() {
        return "Si{" +
                "siId='" + siId + '\'' +
                ", siName='" + siName + '\'' +
                ", siImage='" + siImage + '\'' +
                ", siNickname='" + siNickname + '\'' +
                '}';
    }

    public Si() {
        super();
    }
    public Si(String siId,String siName,String siImage,String siNickname) {
        super();
        this.siId = siId;
        this.siName = siName;
        this.siImage = siImage;
        this.siNickname = siNickname;
    }
    public String getSiId() {
        return this.siId;
    }

    public void setSiId(String siId) {
        this.siId = siId;
    }

    public String getSiName() {
        return this.siName;
    }

    public void setSiName(String siName) {
        this.siName = siName;
    }

    public String getSiImage() {
        return this.siImage;
    }

    public void setSiImage(String siImage) {
        this.siImage = siImage;
    }

    public String getSiNickname() {
        return this.siNickname;
    }

    public void setSiNickname(String siNickname) {
        this.siNickname = siNickname;
    }

}
