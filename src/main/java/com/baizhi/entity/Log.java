package com.baizhi.entity;

import java.util.Date;

/**
 * author: pengSir
 * CreateDate: 2018-08-09
 * TIME: 11:24
 * AIM :
 */
public class Log {

    private Integer id;

    private String methodName;

    private String username;

    private Date createDate;

    private String result;

    @Override
    public String toString() {
        return "Log{" +
                "id=" + id +
                ", methodName='" + methodName + '\'' +
                ", username='" + username + '\'' +
                ", createDate=" + createDate +
                ", result='" + result + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Log(Integer id, String methodName, String username, Date createDate, String result) {
        this.id = id;
        this.methodName = methodName;
        this.username = username;
        this.createDate = createDate;
        this.result = result;
    }

    public Log() {
    }
}
