package com.baizhi.entity;
public class Wen {
    private String wenId;
    private String wenName;
    private String wenAuthor;
    private String wenTeller;
    private Integer wenEpisodes;
    private java.util.Date wenDate;
    private String wenContent;
    private String wenImage;
    private Integer wenStar;
    public Wen() {
        super();
    }
    public Wen(String wenId,String wenName,String wenAuthor,String wenTeller,Integer wenEpisodes,java.util.Date wenDate,String wenContent,String wenImage,Integer wenStar) {
        super();
        this.wenId = wenId;
        this.wenName = wenName;
        this.wenAuthor = wenAuthor;
        this.wenTeller = wenTeller;
        this.wenEpisodes = wenEpisodes;
        this.wenDate = wenDate;
        this.wenContent = wenContent;
        this.wenImage = wenImage;
        this.wenStar = wenStar;
    }
    public String getWenId() {
        return this.wenId;
    }

    public void setWenId(String wenId) {
        this.wenId = wenId;
    }

    public String getWenName() {
        return this.wenName;
    }

    public void setWenName(String wenName) {
        this.wenName = wenName;
    }

    public String getWenAuthor() {
        return this.wenAuthor;
    }

    public void setWenAuthor(String wenAuthor) {
        this.wenAuthor = wenAuthor;
    }

    public String getWenTeller() {
        return this.wenTeller;
    }

    public void setWenTeller(String wenTeller) {
        this.wenTeller = wenTeller;
    }

    public Integer getWenEpisodes() {
        return this.wenEpisodes;
    }

    public void setWenEpisodes(Integer wenEpisodes) {
        this.wenEpisodes = wenEpisodes;
    }

    public java.util.Date getWenDate() {
        return this.wenDate;
    }

    public void setWenDate(java.util.Date wenDate) {
        this.wenDate = wenDate;
    }

    public String getWenContent() {
        return this.wenContent;
    }

    public void setWenContent(String wenContent) {
        this.wenContent = wenContent;
    }

    public String getWenImage() {
        return this.wenImage;
    }

    public void setWenImage(String wenImage) {
        this.wenImage = wenImage;
    }

    public Integer getWenStar() {
        return this.wenStar;
    }

    public void setWenStar(Integer wenStar) {
        this.wenStar = wenStar;
    }

}
