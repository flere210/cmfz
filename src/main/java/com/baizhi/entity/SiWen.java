package com.baizhi.entity;

import java.util.Date;

public class SiWen {
    private String wzId;
    private String wzName;
    private String wzImage;
    private String wzContent;
    private String siId;
    private java.util.Date wzDate;
    private Integer wzCount;

    @Override
    public String toString() {
        return "SiWen{" +
                "wzId='" + wzId + '\'' +
                ", wzName='" + wzName + '\'' +
                ", wzImage='" + wzImage + '\'' +
                ", wzContent='" + wzContent + '\'' +
                ", siId='" + siId + '\'' +
                ", wzDate=" + wzDate +
                ", wzCount=" + wzCount +
                '}';
    }

    public SiWen() {
        super();
    }
    public SiWen(String wzId,String wzName,String wzImage,String wzContent,String siId,java.util.Date wzDate,Integer wzCount) {
        super();
        this.wzId = wzId;
        this.wzName = wzName;
        this.wzImage = wzImage;
        this.wzContent = wzContent;
        this.siId = siId;
        this.wzDate = wzDate;
        this.wzCount = wzCount;
    }

    public SiWen(String title, String content, Date d) {
        this.wzName=title;
        this.wzContent=content;
        this.wzDate=d;
    }

    public String getWzId() {
        return this.wzId;
    }

    public void setWzId(String wzId) {
        this.wzId = wzId;
    }

    public String getWzName() {
        return this.wzName;
    }

    public void setWzName(String wzName) {
        this.wzName = wzName;
    }

    public String getWzImage() {
        return this.wzImage;
    }

    public void setWzImage(String wzImage) {
        this.wzImage = wzImage;
    }

    public String getWzContent() {
        return this.wzContent;
    }

    public void setWzContent(String wzContent) {
        this.wzContent = wzContent;
    }

    public String getSiId() {
        return this.siId;
    }

    public void setSiId(String siId) {
        this.siId = siId;
    }

    public java.util.Date getWzDate() {
        return this.wzDate;
    }

    public void setWzDate(java.util.Date wzDate) {
        this.wzDate = wzDate;
    }

    public Integer getWzCount() {
        return this.wzCount;
    }

    public void setWzCount(Integer wzCount) {
        this.wzCount = wzCount;
    }

}
