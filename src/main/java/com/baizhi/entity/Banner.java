package com.baizhi.entity;

import java.io.Serializable;

public class Banner implements Serializable {
    private Integer bannerId;
    private String bannerImage;
    private String bannerName;
    private String bannerStatus;
    private Integer bannerDec;
    public Banner() {
        super();
    }
    public Banner(Integer bannerId,String bannerImage,String bannerName,String bannerStatus,Integer bannerDec) {
        super();
        this.bannerId = bannerId;
        this.bannerImage = bannerImage;
        this.bannerName = bannerName;
        this.bannerStatus = bannerStatus;
        this.bannerDec = bannerDec;
    }
    public Integer getBannerId() {
        return this.bannerId;
    }

    public void setBannerId(Integer bannerId) {
        this.bannerId = bannerId;
    }

    public String getBannerImage() {
        return this.bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getBannerName() {
        return this.bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    public String getBannerStatus() {
        return this.bannerStatus;
    }

    public void setBannerStatus(String bannerStatus) {
        this.bannerStatus = bannerStatus;
    }

    public Integer getBannerDec() {
        return this.bannerDec;
    }

    public void setBannerDec(Integer bannerDec) {
        this.bannerDec = bannerDec;
    }

}
