package com.baizhi.entity;

import java.util.List;

public class Role {
    private Integer id;
    private String rolename;
    private List<Resource> list;

    public Role() {
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", rolename='" + rolename + '\'' +
                ", list=" + list +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public List<Resource> getList() {
        return list;
    }

    public void setList(List<Resource> list) {
        this.list = list;
    }

    public Role(Integer id, String rolename, List<Resource> list) {
        this.id = id;
        this.rolename = rolename;
        this.list = list;
    }
}
