package com.baizhi.entity;

import java.io.Serializable;
import java.util.List;

public class Resource implements Serializable{
	private Integer id;
	private String text;
	private String state="open";
	private boolean checked;
	private String resUrl;
	private String resType;
	private String resCode;
	private Integer parentId;
	private List<Resource> children;

	@Override
	public String toString() {
		return "Resource{" +
				"id=" + id +
				", text='" + text + '\'' +
				", state='" + state + '\'' +
				", checked=" + checked +
				", resUrl='" + resUrl + '\'' +
				", resType='" + resType + '\'' +
				", resCode='" + resCode + '\'' +
				", parentId=" + parentId +
				", children=" + children +
				'}';
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getResUrl() {
		return resUrl;
	}

	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public List<Resource> getChildren() {
		return children;
	}

	public void setChildren(List<Resource> children) {
		this.children = children;
	}

	public Resource(Integer id, String text, String state, boolean checked, String resUrl, String resType, String resCode, Integer parentId, List<Resource> children) {

		this.id = id;
		this.text = text;
		this.state = state;
		this.checked = checked;
		this.resUrl = resUrl;
		this.resType = resType;
		this.resCode = resCode;
		this.parentId = parentId;
		this.children = children;
	}

	public Resource() {
	}
}
