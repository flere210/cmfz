package com.baizhi.entity;
public class User {
    private Integer userId;
    private String telphone;
    private String password;
    private String userImage;
    private String nickname;
    private String name;
    private String sex;
    private String autograph;
    private String userSheng;
    private String userShi;
    private String guruId;
    public User() {
        super();
    }
    public User(Integer userId,String telphone,String password,String userImage,String nickname,String name,String sex,String autograph,String userSheng,String userShi,String guruId) {
        super();
        this.userId = userId;
        this.telphone = telphone;
        this.password = password;
        this.userImage = userImage;
        this.nickname = nickname;
        this.name = name;
        this.sex = sex;
        this.autograph = autograph;
        this.userSheng = userSheng;
        this.userShi = userShi;
        this.guruId = guruId;
    }
    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTelphone() {
        return this.telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserImage() {
        return this.userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAutograph() {
        return this.autograph;
    }

    public void setAutograph(String autograph) {
        this.autograph = autograph;
    }

    public String getUserSheng() {
        return this.userSheng;
    }

    public void setUserSheng(String userSheng) {
        this.userSheng = userSheng;
    }

    public String getUserShi() {
        return this.userShi;
    }

    public void setUserShi(String userShi) {
        this.userShi = userShi;
    }

    public String getGuruId() {
        return this.guruId;
    }

    public void setGuruId(String guruId) {
        this.guruId = guruId;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", telphone='" + telphone + '\'' +
                ", password='" + password + '\'' +
                ", userImage='" + userImage + '\'' +
                ", nickname='" + nickname + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", autograph='" + autograph + '\'' +
                ", userSheng='" + userSheng + '\'' +
                ", userShi='" + userShi + '\'' +
                ", guruId='" + guruId + '\'' +
                '}';
    }
}
