package com.baizhi.entity.VO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResourceVO {
    private String id;
    private String text;
    private String state;
    private Boolean checked = false;
    private String resourceurl;
    private Map<String,String> attributes = new HashMap<>();
    private List<ResourceVO> children = new ArrayList<>();

    public ResourceVO() {
    }

    public ResourceVO(String id, String text, String state, Boolean checked, String resourceurl, Map<String, String> attributes, List<ResourceVO> children) {
        this.id = id;
        this.text = text;
        this.state = state;
        this.checked = checked;
        this.resourceurl = resourceurl;
        this.attributes = attributes;
        this.children = children;
    }

    @Override
    public String toString() {
        return "ResourceVO{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", state='" + state + '\'' +
                ", checked=" + checked +
                ", resourceurl='" + resourceurl + '\'' +
                ", attributes=" + attributes +
                ", children=" + children +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getResourceurl() {
        return resourceurl;
    }

    public void setResourceurl(String resourceurl) {
        this.resourceurl = resourceurl;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public List<ResourceVO> getChildren() {
        return children;
    }

    public void setChildren(List<ResourceVO> children) {
        this.children = children;
    }
}
