package com.baizhi.entity.VO;

import java.io.Serializable;
import java.util.Date;

public class WenYinVO implements Serializable {
    private String wenId;
    private String wenName;
    private String wenAuthor;
    private String wenTeller;
    private Integer wenEpisodes;
    private Date wenDate;
    private String wenContent;
    private String wenImage;
    private Integer wenStar;
    private String yinId;
    private String yinName;
    private String yinUrl;
    private String yinSize;
    private Integer yinCount;

    public WenYinVO(String wenId, String wenName, String wenAuthor, String wenTeller, Integer wenEpisodes, Date wenDate, String wenContent, String wenImage, Integer wenStar, String yinId, String yinName, String yinUrl, String yinSize, Integer yinCount) {
        this.wenId = wenId;
        this.wenName = wenName;
        this.wenAuthor = wenAuthor;
        this.wenTeller = wenTeller;
        this.wenEpisodes = wenEpisodes;
        this.wenDate = wenDate;
        this.wenContent = wenContent;
        this.wenImage = wenImage;
        this.wenStar = wenStar;
        this.yinId = yinId;
        this.yinName = yinName;
        this.yinUrl = yinUrl;
        this.yinSize = yinSize;
        this.yinCount = yinCount;
    }

    public WenYinVO() {
    }

    @Override
    public String toString() {
        return "WenYinVO{" +
                "wenId='" + wenId + '\'' +
                ", wenName='" + wenName + '\'' +
                ", wenAuthor='" + wenAuthor + '\'' +
                ", wenTeller='" + wenTeller + '\'' +
                ", wenEpisodes=" + wenEpisodes +
                ", wenDate=" + wenDate +
                ", wenContent='" + wenContent + '\'' +
                ", wenImage='" + wenImage + '\'' +
                ", wenStar=" + wenStar +
                ", yinId='" + yinId + '\'' +
                ", yinName='" + yinName + '\'' +
                ", yinUrl='" + yinUrl + '\'' +
                ", yinSize='" + yinSize + '\'' +
                ", yinCount=" + yinCount +
                '}';
    }

    public String getWenId() {
        return wenId;
    }

    public void setWenId(String wenId) {
        this.wenId = wenId;
    }

    public String getWenName() {
        return wenName;
    }

    public void setWenName(String wenName) {
        this.wenName = wenName;
    }

    public String getWenAuthor() {
        return wenAuthor;
    }

    public void setWenAuthor(String wenAuthor) {
        this.wenAuthor = wenAuthor;
    }

    public String getWenTeller() {
        return wenTeller;
    }

    public void setWenTeller(String wenTeller) {
        this.wenTeller = wenTeller;
    }

    public Integer getWenEpisodes() {
        return wenEpisodes;
    }

    public void setWenEpisodes(Integer wenEpisodes) {
        this.wenEpisodes = wenEpisodes;
    }

    public Date getWenDate() {
        return wenDate;
    }

    public void setWenDate(Date wenDate) {
        this.wenDate = wenDate;
    }

    public String getWenContent() {
        return wenContent;
    }

    public void setWenContent(String wenContent) {
        this.wenContent = wenContent;
    }

    public String getWenImage() {
        return wenImage;
    }

    public void setWenImage(String wenImage) {
        this.wenImage = wenImage;
    }

    public Integer getWenStar() {
        return wenStar;
    }

    public void setWenStar(Integer wenStar) {
        this.wenStar = wenStar;
    }

    public String getYinId() {
        return yinId;
    }

    public void setYinId(String yinId) {
        this.yinId = yinId;
    }

    public String getYinName() {
        return yinName;
    }

    public void setYinName(String yinName) {
        this.yinName = yinName;
    }

    public String getYinUrl() {
        return yinUrl;
    }

    public void setYinUrl(String yinUrl) {
        this.yinUrl = yinUrl;
    }

    public String getYinSize() {
        return yinSize;
    }

    public void setYinSize(String yinSize) {
        this.yinSize = yinSize;
    }

    public Integer getYinCount() {
        return yinCount;
    }

    public void setYinCount(Integer yinCount) {
        this.yinCount = yinCount;
    }
}
