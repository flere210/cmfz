package com.baizhi.entity.VO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoleVO implements Serializable {
    private int id;
    private String text;

    public RoleVO() {
    }

    public RoleVO(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "RoleTree{" +
                "id=" + id +
                ", text='" + text + '\'' +
                '}';
    }
}