package com.baizhi.realm;


import com.baizhi.dao.AdminDao;
import com.baizhi.dao.RoleDao;
import com.baizhi.entity.Admin;
import com.baizhi.entity.Resource;
import com.baizhi.entity.Role;
import com.baizhi.service.RoleService;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * author: pengSir
 * CreateDate: 2018-08-13
 * TIME: 11:32
 * AIM : 自定义认证realm
 */
public class MyRealm extends AuthorizingRealm {
        /**
         * 认证账号方法
         * @param authenticationToken
         * @return
         * @throws AuthenticationException
         */
        @Autowired
        private AdminDao adminDao;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleDao roleDao;

        @Override
        protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
            Admin admin =new Admin();
            // 1. 获取用户输入的账号  authenticationToken
            String username = authenticationToken.getPrincipal().toString();
            admin.setUsername(username);

            Admin shiroAdmin = adminDao.selectAdminByObj(admin);
            //2. 根据账号  查询数据库中是否存在当前用户
            if(shiroAdmin!=null){
                    // 3. 将 密码数据交于 shiro 方便之后的密码校验
                    return new SimpleAuthenticationInfo(username,new Md5Hash("123456","OMGG",1),
                            ByteSource.Util.bytes(shiroAdmin.getPasswordsalt()),this.getName());
            }
            return null;
        }

        //授权
        @Override
        protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
            // 1. 获取用户名
            String username = principalCollection.getPrimaryPrincipal().toString();
            // 2. 根据用户名获取权限数据

            List<Role> roles = roleService.selectRoleByAdmin(username);
            List<String> p = new ArrayList<>();
            for(Role role:roles){
                //根据角色查询对应资源
                List<Resource> resources = roleDao.selectResourceByName(role.getRolename());
                System.out.println(resources);
                for(Resource resource:resources){
                    if (resource.getResCode()!=null)
                        p.add(resource.getResCode());
                }
            }

            // 把权限数据交于shiro
            SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

            info.addStringPermissions(p);

            return info;
        }
}
