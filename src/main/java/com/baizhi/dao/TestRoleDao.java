package com.baizhi.dao;

import com.baizhi.entity.Resource;
import com.baizhi.entity.Role;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * author: pengSir
 * CreateDate: 2018-08-14
 * TIME: 14:22
 * AIM : 角色
 */
public interface TestRoleDao {
    //   根据角色名查询顶级资源
    List<Resource> selectParentResourceByRoleName(String roleName);

    // 查询当前资源的子级资源
    List<Resource> selectSonResource(@Param("parentId") Integer parentId, @Param("roleName") String roleName);

    // 查询某一个角色下的资源并且封装到角色的资源集合属性中

    /**
     * 查询所有角色
     */
    List<Role> selectAllRole();
}
