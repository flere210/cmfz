package com.baizhi.dao;
import com.baizhi.entity.Log;
import org.apache.ibatis.annotations.Param;

import java.util.List;
public interface LogDao{
	/**
	 * 获得Log数据的总行数
	 * @return
	 */
    long getLogRowCount();
	/**
	 * 获得Log数据集合
	 * @return
	 */
    List<Log> selectLog();
	/**
	 * 获得一个Log对象,以参数Log对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    Log selectLogByObj(Log obj);
	/**
	 * 通过Log的id获得Log对象
	 * @param id
	 * @return
	 */
    Log selectLogById(Integer id);
	/**
	 * 插入Log到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertLog(Log value);
	/**
	 * 插入Log中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptyLog(Log value);
	/**
	 * 通过Log的id删除Log
	 * @param id
	 * @return
	 */
    int deleteLogById(Integer id);
	/**
	 * 通过Log的id更新Log中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateLogById(Log enti);
	/**
	 * 通过Log的id更新Log中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptyLogById(Log enti);

    List<Log> selectLogBypage(@Param("start") int start, @Param("rows") Integer rows);
}