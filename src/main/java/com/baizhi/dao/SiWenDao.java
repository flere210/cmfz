package com.baizhi.dao;
import com.baizhi.entity.SiWen;
import org.apache.ibatis.annotations.Param;

import java.rmi.activation.ActivationID;
import java.util.List;
public interface SiWenDao{
	/**
	 * 获得SiWen数据的总行数
	 * @return
	 */
    long getSiWenRowCount();
	/**
	 * 获得SiWen数据集合
	 * @return
	 */
    List<SiWen> selectSiWen();
	/**
	 * 获得一个SiWen对象,以参数SiWen对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    SiWen selectSiWenByObj(SiWen obj);
	/**
	 * 通过SiWen的id获得SiWen对象
	 * @param id
	 * @return
	 */
    SiWen selectSiWenById(String id);
	/**
	 * 插入SiWen到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertSiWen(SiWen value);
	/**
	 * 插入SiWen中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptySiWen(SiWen value);
	/**
	 * 通过SiWen的id删除SiWen
	 * @param id
	 * @return
	 */
    int deleteSiWenById(String id);
	/**
	 * 通过SiWen的id更新SiWen中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateSiWenById(SiWen enti);
	/**
	 * 通过SiWen的id更新SiWen中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptySiWenById(SiWen enti);

    List<SiWen> selectSiWenBySid(String sid);

    int removeBatch(@Param("aid") String[] ids);

	List selectSiWenBypage(@Param("page") Integer page,@Param("rows") Integer rows);
}