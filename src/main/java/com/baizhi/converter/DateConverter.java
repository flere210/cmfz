package com.baizhi.converter;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter implements Converter<String, Date> {
    /**
     * String请求参数----->Date类型
     * 参数: 浏览器需要被转化的请求参数
     * 返回值: 被转化后的,可以给controller使用的值.
     */
    @Override
    public Date convert(String str) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(str);
        } catch (ParseException e) {
            throw new RuntimeException("日期转化异常",e);
        }
        return date;
    }
}