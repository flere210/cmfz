<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'cmfz_listen.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">
	  <script type="text/javascript">
          $(function () {
              $("#showAllcmfz_listen").datagrid({
                  url: "${pageContext.request.contextPath}/listen/",
                  title: '专辑管理',
                  remoteSort: false,
                  nowrap: false,
                  pagination: true,
                  pageSize: 5,
                  pageList: [5, 10, 20, 50],
                  fitColumns: true,
                  toolbar: "#btn",
                  method: "get",
                  columns: [[
                      {field: 'ck', title: '选择', checkbox: true},
                      {field: 'wenId', title: 'Item ID', width: 80},
                      {field: 'wenName', title: 'Product ID', width: 100, sortable: true},
                      {field: 'wenAuthor', title: 'List Price', width: 80, align: 'right', sortable: true},
                      {field: 'wenTeller', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'wenEpisodes', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'wenDate', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'wenContent', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'wenImage', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'wenStar', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                  ]],
                  view: detailview,
                  detailFormatter: function (rowIndex, rowData) {
                      return '<table><tr>' +
                          '<td rowspan=2 style="border:0"><img src="picture/' + rowData.wenImage + '" style="height:50px;"></td>' +
                          '<td style="border:0">' +
                          '</td>' +
                          '</tr></table>';
                  }
              });
          })

          function add() {
              $("#addListen").dialog('open')
          }

          function addlisten() {
              $("#addListenInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/listen/addlisten",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#addListen').dialog('close');
                      $('#addListenInformation').form('clear');
                      $('#showAllcmfz_listen').datagrid('reload');
                  }
              })
          }

          function removeBatch() {
              var ps = $("#showAllcmfz_listen").datagrid('getChecked');
              var wid = "";
              $.each(ps, function (i, p) {
                  wid += "&wid=" + p.wenId;
              });
              wid = wid.substring(1);
              console.log(wid);
              $.ajax({
                  url: "${pageContext.request.contextPath}/listen/removeBatch?"+wid,
                  dataType: "json",
                  method: "get",
                  success: function (obj) {
                      $('#showAllcmfz_listen').datagrid('reload');

                  }

              });
          }

          function update() {
              var ps=$("#showAllcmfz_listen").datagrid('getChecked');
              var wid="";
              $.each(ps,function (i,p) {
                  wid = p.wenId
              });

              console.log(wid);
              $("#updateListenInformation").form('load',"${pageContext.request.contextPath}/listen/selectOne?wid="+wid)
              $("#updateListen").dialog('open')
          }


          function updatelisten() {
              $("#addListenInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/listen/Update",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#updateListen').dialog('close');
                      $('#addListenInformation').form('clear');
                      $('#showAllcmfz_listen').datagrid('reload');
                  }
              })
          }
	  </script>

  </head>
  
  <body>
  <div id="btn">
	  <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'"
		 onclick="removeBatch()">批量删除</a>
	  <a href="javascript:void(o)" class="easyui-linkbutton" onclick="add()" data-options="iconCls:'icon-add'">添加</a>
	  <a href="javascript:void(o)" class="easyui-linkbutton" onclick="update()" data-options="iconCls:'icon-add'">修改</a>
  </div>
    <table id="showAllcmfz_listen"></table>
    <div id="addListen" class="easyui-dialog" data-options="closed:true">
    	<form id="addListenInformation" enctype="multipart/form-data" method="post">
    		<br>
    		专辑名：&nbsp;<input name="wenName"><br>
    		专辑封面：<input name="uploadFile" class="easyui-filebox" data-options="
    			buttonText:'选择文件',
    			width:160,
    			height:20
    		"><br>
    		专辑作者：<input name="wenAuthor"><br>
    		播音员：&nbsp;<input name="wenTeller"><br>
    				
    		专辑评分：<input name="wenStar" class="easyui-numberbox" data-options="min:0,width:160,height:20"><br>
    		专辑集数：<input name="wenEpisodes" class="easyui-numberbox" data-options="min:0,width:160,height:20"><br>
    		
    		出版时间：<input name="wenDate"  class="easyui-datebox" data-options="width:160,height:20"><br>
    		专辑简介：<br>
    				&nbsp;&nbsp;&nbsp;
    				<textarea name="wenContent" rows="5px" cols="20px"></textarea>
			<center>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="addlisten()">确认</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
			</center>
		</form>

    </div>
    <div id="updateListen" class="easyui-dialog" data-options="closed:true">
    	<form id="updateListenInformation" method="post">
    		<br>
    				<input id="listen_id" name="wenId" style="display: none"> <%--enctype="multipart/form-data" --%>
    		专辑名：&nbsp;<input name="wenName" id="listen_name"><br>
    		<%--专辑封面：<input name="uploadFile" class="easyui-filebox" data-options="
    			buttonText:'选择文件'
    		"><br>--%>
    		专辑作者11：<input name="wenAuthor" id="listen_author"><br>
    		播音员：&nbsp;<input name="wenTeller" id="listen_announcer"><br>
    				
    		专辑评分：<input name="wenStar" id="listen_star" class="easyui-numberbox" data-options="min:0"><br>
    		专辑集数：<input name="wenEpisodes" id="listen_amount" class="easyui-numberbox" data-options="min:0"><br>
    		专辑简介：<br>
    				      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    				      <textarea name="wenContent" id="listen_brief_introduction" rows="5px" cols="20px"></textarea>
			<center>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="updatelisten()">确认</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
			</center>
		</form>
    		<p id="listen_picture"></p>
    </div>
	<script type="text/javascript">

	</script>
  </body>
</html>
