<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'cmfz_userlog.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">

	  <script type="text/javascript">
		  $(function () {
              $("#showAllcmfz_userlog").datagrid({
                  url: "${pageContext.request.contextPath}/Log/",
                  method:"get",
                  title: '日志管理',
                  remoteSort: false,
                  nowrap: false,
                  pagination: true,
                  pageSize: 5,
                  pageList: [5, 10, 20, 50],
                  fitColumns: true,
                  method: "get",
                  columns: [[

                      {field: 'id', title: 'Item ID', width: 80},
                      {field: 'methodName', title: 'Product ID', width: 100, sortable: true},
                      {field: 'username', title: 'List Price', width: 80, align: 'right', sortable: true},
                      {field: 'createDate', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'result', title: 'Attribute', width: 150, sortable: true},
                  ]],
              })
          })

	</script>
  </head>
  <body>
    <table id="showAllcmfz_userlog"></table>
	</body>
</html>
