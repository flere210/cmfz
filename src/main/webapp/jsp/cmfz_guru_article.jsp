<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'cmfz_guru_article.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">
    <script>
        function addIndexDB() {
            $.ajax({
                url: "${pageContext.request.contextPath}/SiWen",
                type: "post",
                data: "",
                dataType: "jsom",
                success: function (obj) {

                }
            })

        }
        function resetIndexDB() {
            $.ajax({
                url: "${pageContext.request.contextPath}/SiWen/removeIndex",
                type: "post",
                dataType: "json",
                data:"",
                success: function (obj) {
                    $("#addIndexDB").linkbutton({
                        disabled:false,
                    });
                }
            })
        }

        function lucen() {
            var keysword = $("#keysword").val();
            console.log(keysword)
            $("#showAllcmfz_guru_article").datagrid({
                url: "${pageContext.request.contextPath}/SiWen/" + keysword,
                pagination: true,
                pageSize: 3,
                pageList: [3, 6, 9],
                onLoadError: function () {
                    alert("先创建索引库")
                },
                columns: [[
                    {field: 'ck', title: '选择', checkbox: true},
                    {field: 'wzId', title: '文章id', width: 80},
                    {field: 'wzName', title: '文章标题', width: 100, sortable: true},
                    {field: 'wzImage', title: '文章图片', width: 80, align: 'right', sortable: true},
                    {field: 'wzContent', title: '文章内容', width: 80, align: 'right', sortable: true},
                    {field: 'siId', title: '上师id', width: 150, sortable: true},
                    {field: 'wzDate', title: '创建时间', width: 150, sortable: true},
                ]],
                view: detailview,
                detailFormatter: function (rowIndex, rowData) {
                    return '<table><tr>' +
                        '<td rowspan=2 style="border:0"><img src="picture/' + rowData.wzImage + '" style="height:50px;"></td>' +
                        '<td style="border:0">' +
                        '</td>' +
                        '</tr></table>';
                }
            })
        }

        //展示添加
        function addGuruArticle() {
            $("#addArticle").dialog('open')
            $("#siid1").combobox({
                panelHeight:100,
                panelWidth:150,
                url:"${pageContext.request.contextPath}/guru/ShowSi",

                valueField:"siId",
                textField:"siNickname",
            })

        }

        //展示修改
        function updateGuruArticle() {

            var ps=$("#showAllcmfz_guru_article").datagrid('getChecked');
            var wzid="";
            $.each(ps,function (i,p) {
                wzid = p.wzId
            });
            $("#siid2").combobox({
                panelHeight:100,
                panelWidth:150,
                url:"${pageContext.request.contextPath}/Si/ShowSi",
                valueField:"siId",
                textField:"siNickname",
            })
            console.log(wzid);
            $("#updateguru_ArticleInformation").form('load',"${pageContext.request.contextPath}/SiWen/selectOne?wzid="+wzid)
            $("#updateguru_Article").dialog('open')
        }

        function updateArticle() {
            var siName =  $("#siid2").combobox('getText');
            console.log(siName)
            var ps=$("#showAllcmfz_guru_article").datagrid('getChecked');
            var imgsrc="";
            $.each(ps,function (i,p) {
                imgsrc = p.wzImage
            });
            console.log(imgsrc)
            $("#updateguru_ArticleInformation").form('submit', {
                url: "${pageContext.request.contextPath}/SiWen/Update?siName="+siName,
                onSubmit: function () {
                    return true;
                },
                success: function (str) {

                    $('#updateguru_Article').dialog('close');
                    $('#updateguru_ArticleInformation').form('clear');
                    $('#showAllcmfz_guru_article').datagrid('reload');
                }
            })
        }

        function addArticle() {
            var siName =  $("#siid1").combobox('getText');
            console.log(siName+"sadadasd")
            $("#addArticleInformation").form('submit', {
                url: "${pageContext.request.contextPath}/SiWen/Add?siName="+siName,
                onSubmit: function () {
                    return true;
                },
                success: function (str) {

                    $('#addArticle').dialog('close');
                    $('#addArticleInformation').form('clear');
                    $('#showAllcmfz_guru_article').datagrid('reload');
                }
            })
        }

        function removeGuruArticle() {
            var ps = $("#showAllcmfz_guru_article").datagrid('getChecked');
            var aid = "";
            $.each(ps, function (i, p) {
                aid += "&aid="+ p.wzId;
            });
            aid = aid.substring(1);
            console.log(aid);
            $.ajax({
                url: "${pageContext.request.contextPath}/SiWen/removeBatch?"+aid,
                dataType: "json",
                method: "get",
                success: function (obj) {
                    $('#showAllcmfz_guru_article').datagrid('reload');
                }

            });
        }

    </script>
</head>
<body>
<table id="showAllcmfz_guru_article">
    <div id="lala">
        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-add'"
           onClick="addGuruArticle();">添加</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-edit'"
           onClick="updateGuruArticle();">修改</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-remove'"
           onClick="removeGuruArticle();">删除</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-remove'"
           id="resetIndexDB" onClick="resetIndexDB();">重置索引库</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-remove',disabled:true"
           id="addIndexDB" onClick="addIndexDB();">添加索引库</a>
        <input class="easyui-searchbox" id="keysword" data-options="prompt:'Please Input Value',searcher:''"
               style="width:130px;vertical-align:middle;"></input>
        <a href="javascript:void(0)" onClick="lucen();" class="easyui-linkbutton" plain="true">高级检索</a>
    </div>

</table>

<div id="addArticle" class="easyui-dialog" data-options="closed:true">
    <form id="addArticleInformation" enctype="multipart/form-data" method="post">
        文章标题：<input name="wzName"><br>
        所属上师：<input id="siid1" name="siId" class="easyui-combobox"><br>
        文章插图：<input name="uploadFile" class="easyui-filebox" data-options="
    			buttonText:'选择文件'
    		"><br>
        文章内容：<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea name="wzContent" rows="5px" cols="30px"></textarea>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="addArticle()">确认</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
    </form>
</div>
<div id="updateguru_Article" class="easyui-dialog" data-options="closed:true">
    <form id="updateguru_ArticleInformation" enctype="multipart/form-data" method="post">
        <input id="article_id" name="article_id" style="display: none">
        <input type="hidden" name="wzId">
        文章标题：<input id="wzName" name="wzName"><br>
        所属上师：<input id="siid2" name="siId" class="easyui-combobox"><br>
        文章插图：<input name="uploadFile" class="easyui-filebox" data-options="
    			buttonText:'选择文件'
    		"><br>
        阅读量：<input name="wzCount" id="article_read_count" type="text" class="easyui-numberbox"
                   data-options="min:0"></input>
        文章内容：<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea id="article_content" name="wzContent" rows="5px"
                                                cols="30px"></textarea>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="updateArticle()">确认</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>

    </form>
    <p id="Article_picture"></p>
</div>
</body>
<script type="text/javascript">
    $(function () {
        $("#showAllcmfz_guru_article").datagrid({
            url: "${pageContext.request.contextPath}/SiWen/",
            fitColumns: true,
            striped: true,
            pagination: true,
            pageSize: 5,
            pageList: [5, 10, 20, 50],
            method: "get",
            columns: [[
                {field: 'ck', title: '选择', checkbox: true},
                {field: 'wzId', title: '文章id', width: 80},
                {field: 'wzName', title: '文章标题', width: 100, sortable: true},
                {field: 'wzImage', title: '文章图片', width: 80, align: 'right', sortable: true},
                {field: 'wzContent', title: '文章内容', width: 80, align: 'right', sortable: true},
                {field: 'siId', title: '上师id', width: 150, sortable: true},
                {field: 'wzDate', title: '创建时间', width: 150, sortable: true},
            ]],
            view: detailview,
            detailFormatter: function (rowIndex, rowData) {
                return '<table><tr>' +
                    '<td rowspan=2 style="border:0"><img src="picture/' + rowData.wzImage + '" style="height:50px;"></td>' +
                    '<td style="border:0">' +
                    '</td>' +
                    '</tr></table>';
            }
        })


    })
</script>
</html>
