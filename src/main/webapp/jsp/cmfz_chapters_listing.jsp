<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'cmfz_chapters_listing.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">
	  <script type="text/javascript">
          $(function () {
              $("#showAllcmfz_chapters_listing").datagrid({
                  url: "${pageContext.request.contextPath}/WenYin/",
                  title: '音频管理',
                  remoteSort: false,
                  nowrap: false,
                  pagination: true,
                  pageSize: 5,
                  pageList: [5, 10, 20, 50],
                  fitColumns: true,
                  toolbar: "#btn",
                  method: "get",
                  columns: [[
                      {field: 'ck', title: '选择', checkbox: true},
                      {field: 'yinId', title: 'Item ID', width: 80},
                      {field: 'yinName', title: 'Product ID', width: 100, sortable: true},
                      {field: 'wenId', title: 'List Price', width: 80, align: 'right', sortable: true},
                      {field: 'yinUrl', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'yinSize', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'yinCount', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                  ]],

              });
          })

          function add() {
              $("#addchapters_listing").dialog('open');

          }

          function addyin() {
              $("#addchapters_listingInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/WenYin/addlisten",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#addchapters_listing').dialog('close');
                      $('#addchapters_listingInformation').form('clear');
                      $('#showAllcmfz_chapters_listing').datagrid('reload');
                  }
              })
          }

          function removeBatch() {
              var ps = $("#showAllcmfz_chapters_listing").datagrid('getChecked');
              var yid = "";
              $.each(ps, function (i, p) {
                  yid += "&yid=" + p.yinId;
              });
              yid = yid.substring(1);
              console.log(yid);
              $.ajax({
                  url: "${pageContext.request.contextPath}/WenYin/removeBatch?"+yid,
                  dataType: "json",
                  method: "get",
                  success: function (obj) {
                      $('#showAllcmfz_chapters_listing').datagrid('reload');

                  }

              });
          }

          function update() {
              var ps=$("#showAllcmfz_chapters_listing").datagrid('getChecked');
              var yid="";
              $.each(ps,function (i,p) {
                  yid = p.yinId
              });

              console.log(yid);
              $("#updatechapters_listingInformation").form('load',"${pageContext.request.contextPath}/WenYin/selectOne?yid="+yid)
              $("#updatechapters_listing").dialog('open')
          }
          function updateyin() {
              $("#updatechapters_listingInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/WenYin/Update",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#updatechapters_listing').dialog('close');
                      $('#updateguruInformation').form('clear');
                      $('#showAllcmfz_chapters_listing').datagrid('reload');
                  }
              })
          }
	  </script>

  </head>
  
  <body>
    <table id="showAllcmfz_chapters_listing"></table>
	<div id="btn">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'"
		   onclick="removeBatch()">批量删除</a>
		<a href="javascript:void(o)" class="easyui-linkbutton" onclick="add()" data-options="iconCls:'icon-add'">添加</a>
		<a href="javascript:void(o)" class="easyui-linkbutton" onclick="update()" data-options="iconCls:'icon-add'">修改</a>
	</div>
    <div id="addchapters_listing" class="easyui-dialog" data-options="closed:true">
    	<form id="addchapters_listingInformation" enctype="multipart/form-data" method="post">
    		<br>
    		章节名：&nbsp;<input name="yinName"><br>
    		章节音频：<input name="uploadFile" class="easyui-filebox" data-options="
    			buttonText:'选择文件'
    		"><br>
    		所属专辑：<input name="wenId" class="easyui-combobox" /><br>
    		章节大小：<input name="yinSize" class="easyui-numberbox" data-options="min:0">M<br>
    		章节下载次数：<input name="yinCount" class="easyui-numberbox" data-options="min:0"><br>
			<center>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="addyin()">确认</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
			</center>
    		
    	</form>
    </div>
    <div id="updatechapters_listing" class="easyui-dialog" data-options="closed:true">
    	<form id="updatechapters_listingInformation" enctype="multipart/form-data" method="post">
    		<br>
    				<input id="chapter_id" name="yinId" style="display: none">
    		章节名：&nbsp;<input id="chapter_name" name="yinName"><br>
    		章节音频：<input name="uploadFile" class="easyui-filebox" data-options="
    			buttonText:'选择文件'
    		"><br>
    		所属专辑：<input id="listen_id" name="wenId" class="easyui-combobox"/><br>
    		章节大小：<input id="chapter_size" name="yinSize" class="easyui-numberbox" data-options="min:0">M<br>
    		章节下载次数：<input id="chapter_download_count" name="yinCount" class="easyui-numberbox" data-options="min:0"><br>
			<center>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="updateyin()">确认</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
			</center>

    	</form>
    		<p id="listen_picture"></p>
    </div>
		<script type="text/javascript">
          
		</script>
  </body>
</html>
