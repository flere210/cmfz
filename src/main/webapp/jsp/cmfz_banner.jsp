<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'cmfz_banner.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">
    <script>
        $(function () {

            $("#showAllcmfz_banner").datagrid({
                url: "${pageContext.request.contextPath}/banner/",
                title: '轮播图管理',
                remoteSort: false,
                nowrap: false,
                pagination: true,
                pageSize: 5,
                pageList: [5, 10, 20, 50],
                fitColumns: true,
                toolbar: "#btn",
                method: "get",
                columns: [[
                    {field: 'ck', title: '选择', checkbox: true},
                    {field: 'bannerId', title: 'Item ID', width: 80},
                    {field: 'bannerImage', title: 'Product ID', width: 100, sortable: true},
                    {field: 'bannerName', title: 'List Price', width: 80, align: 'right', sortable: true},
                    {field: 'bannerStatus', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                    {field: 'bannerDec', title: 'Attribute', width: 150, sortable: true},
                ]],
                view: detailview,
                detailFormatter: function (rowIndex, rowData) {
                    return '<table><tr>' +
                        '<td rowspan=2 style="border:0"><img src="picture/' + rowData.bannerImage + '" style="height:50px;"></td>' +
                        '<td style="border:0">' +
                        '</td>' +
                        '</tr></table>';
                }
            });
        })

        function add() {
            $("#addBanner").dialog('open')
        }

        function addBanner() {
            $("#addform").form('submit', {
                url: "${pageContext.request.contextPath}/banner/",
                onSubmit: function () {
                    return true;
                },
                success: function (str) {

                    $('#addBanner').dialog('close');
                    $('#addform').form('clear');
                    $('#showAllcmfz_banner').datagrid('reload');
                    $.messager.show({
                        title: "商品操作",
                        msg: '添加成功',
                        timeout: 2000
                    });

                }
            })
        }

        function removeBatch() {
            var ps = $("#showAllcmfz_banner").datagrid('getChecked');
            var aid = "";
            $.each(ps, function (i, p) {
                aid += "-" + p.bannerId;
            });
            aid = aid.substring(1);
            console.log(aid);
            $.ajax({
                url: "${pageContext.request.contextPath}/banner/",
                dataType: "json",
                method: "post",
                data: {
                    _method: "delete",
                    aid: aid
                },
                success: function (obj) {
                    alert(1)
                    $('#showAllcmfz_banner').datagrid('reload');

                }

            });
        }

        function update() {
            var ps=$("#showAllcmfz_banner").datagrid('getChecked');
            var wzid="";
            $.each(ps,function (i,p) {
                wzid = p.bannerId
            });

            console.log(wzid);
            $("#updateform").form('load',"${pageContext.request.contextPath}/banner/selectOne?bid="+wzid)
            $("#updateBanner").dialog('open')
        }
        function updateBanner() {

            $("#updateform").form('submit', {
                url: "${pageContext.request.contextPath}/banner/Update",
                onSubmit: function () {
                    return true;
                },
                success: function (str) {

                    $('#updateBanner').dialog('close');
                    $('#updateform').form('clear');
                    $('#showAllcmfz_banner').datagrid('reload');
                }
            })
        }
    </script>

</head>

<body>

<table id="showAllcmfz_banner"></table>
<div id="btn">
    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'"
       onclick="removeBatch()">批量删除</a>
    <a href="javascript:void(o)" class="easyui-linkbutton" onclick="add()" data-options="iconCls:'icon-add'">添加</a>
    <a href="javascript:void(o)" class="easyui-linkbutton" onclick="update()" data-options="iconCls:'icon-add'">修改</a>
</div>
<div id="addBanner" class="easyui-dialog" data-options="closed:true">
    <form id="addform" style="margin:20px" enctype="multipart/form-data" method="post">
        轮播图名字：&nbsp;&nbsp;&nbsp;&nbsp;<input class="easyui-validatebox" name="bannerName"
                                             data-options="required:true,missingMessage:'轮播图名字'"/><br/>
        轮播图图片：&nbsp;&nbsp;&nbsp;&nbsp;<input class="easyui-filebox" name="uploadFile"
                                             data-options="required:true,missingMessage:'请选择封面'"/><br/><br/>
        是否展示：&nbsp;&nbsp;&nbsp;&nbsp;<input class="easyui-validatebox" name="bannerStatus"
                                            data-options="required:true,missingMessage:'是否展示'"/><br/>
        <center>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="addBanner()">确认</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
        </center>
    </form>
</div>
    <div id="updateBanner" class="easyui-dialog" data-options="closed:true">
        <form id="updateform" style="margin:20px" enctype="multipart/form-data" method="post">
            <input type="hidden" name="bannerId">
            轮播图名字：&nbsp;&nbsp;&nbsp;&nbsp;<input class="easyui-validatebox" name="bannerName"
                                                 data-options="required:true,missingMessage:'轮播图名字'"/><br/>
            轮播图图片：&nbsp;&nbsp;&nbsp;&nbsp;<input class="easyui-filebox" name="uploadFile"
                                                 data-options="required:true,missingMessage:'请选择封面'"/><br/><br/>
            是否展示：&nbsp;&nbsp;&nbsp;&nbsp;<input class="easyui-validatebox" name="bannerStatus"
                                                data-options="required:true,missingMessage:'是否展示'"/><br/>
            <center>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="updateBanner()">确认</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
            </center>
        </form>
</div>
</body>
</html>
