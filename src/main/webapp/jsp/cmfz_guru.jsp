<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'cmfz_guru.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">
	  <script type="text/javascript">
          $(function () {
              $("#showAllcmfz_guru").datagrid({
                  url: "${pageContext.request.contextPath}/guru/",
                  title: '上师管理',
                  remoteSort: false,
                  nowrap: false,
                  pagination: true,
                  pageSize: 5,
                  pageList: [5, 10, 20, 50],
                  fitColumns: true,
                  toolbar: "#btn",
                  method: "get",
                  columns: [[
                      {field: 'ck', title: '选择', checkbox: true},
                      {field: 'siId', title: 'Item ID', width: 80},
                      {field: 'siName', title: 'Product ID', width: 100, sortable: true},
                      {field: 'siImage', title: 'List Price', width: 80, align: 'right', sortable: true},
                      {field: 'siNickname', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                  ]],
                  view: detailview,
                  detailFormatter: function (rowIndex, rowData) {
                      return '<table><tr>' +
                          '<td rowspan=2 style="border:0"><img src="picture/' + rowData.siImage + '" style="height:50px;"></td>' +
                          '<td style="border:0">' +
                          '</td>' +
                          '</tr></table>';
                  }
              });
          })

          function add() {
              $("#addguru").dialog('open')
          }

          function addguru() {
              $("#addguruInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/guru/addguru",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#addguru').dialog('close');
                      $('#addguruInformation').form('clear');
                      $('#showAllcmfz_guru').datagrid('reload');
                  }
              })
          }

          function removeBatch() {
              var ps = $("#showAllcmfz_guru").datagrid('getChecked');
              var aid = "";
              $.each(ps, function (i, p) {
                  aid += "&aid=" + p.siId;
              });
              aid = aid.substring(1);
              console.log(aid);
              $.ajax({
                  url: "${pageContext.request.contextPath}/guru/removeBatch?"+aid,
                  dataType: "json",
                  method: "get",
                  success: function (obj) {
                      $('#showAllcmfz_guru').datagrid('reload');

                  }

              });
          }

          function update() {
              var ps=$("#showAllcmfz_guru").datagrid('getChecked');
              var sid="";
              $.each(ps,function (i,p) {
                  sid = p.siId
              });

              console.log(sid);
              $("#updateguruInformation").form('load',"${pageContext.request.contextPath}/guru/selectOne?sid="+sid)
              $("#updateguru").dialog('open')
          }
		  function updateguru() {
              $("#updateguruInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/guru/Update",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#updateguru').dialog('close');
                      $('#updateguruInformation').form('clear');
                      $('#showAllcmfz_guru').datagrid('reload');
                  }
              })
          }
	  </script>

  </head>
  
  <body>
    <table id="showAllcmfz_guru"></table>
	<div id="btn">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'"
		   onclick="removeBatch()">批量删除</a>
		<a href="javascript:void(o)" class="easyui-linkbutton" onclick="add()" data-options="iconCls:'icon-add'">添加</a>
		<a href="javascript:void(o)" class="easyui-linkbutton" onclick="update()" data-options="iconCls:'icon-add'">修改</a>
	</div>
    <div id="addguru" class="easyui-dialog" data-options="closed:true">
    	<form id="addguruInformation" enctype="multipart/form-data" method="post">
    		上师名字：<input name="siName"><br>
    		上师法号：<input name="siNickname"><br>
    		上师照片：<input name="uploadFile" class="easyui-filebox" data-options="
    			buttonText:'选择文件',
    			width:150,
    			height:20
    		">
			<center>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="addguru()">确认</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
			</center>
    	</form>
    </div>
    <div id="updateguru" class="easyui-dialog" data-options="closed:true">
    	<form id="updateguruInformation" enctype="multipart/form-data" method="post">
    				<input id="guru_id" name="siId" style="display: none">
    		上师名字：<input id="guru_name" name="siName"><br>
    		上师法号：<input name="siNickname"><br>
    		上师照片：<input name="uploadFile" class="easyui-filebox" data-options="
    			buttonText:'选择文件',
    			width:150,
    			height:20
    		">
			<center>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="updateguru()">确认</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
			</center>
    	</form>
    		<p id="guru_picture"></p>
    </div>

  </body>
</html>
