<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">

	<title>My JSP 'cmfz_listen.jsp' starting page</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">
	<script type="text/javascript">
        $(function () {
            $("#showAllcmfz_user").datagrid({
                url:"${pageContext.request.contextPath}/admins/queryAllAdmins",
                singleSelect:true,
                pagination:true,
                pageSize:3,
                pageList:[3,6,9],
                toolbar:"#lele",
                columns:[[
                    {field:"",title:"",checkbox:true},
                    {field:"id",title:"图片ID",width:50,align:"center",hidden:true},
                    {field:"username",title:"用户名字",width:150,align:"center"},
                    {field:"beforepassword",title:"用户密码",width:150,align:"center"},
                    {field:"passwordsalt",title:"盐值",width:150,align:"center"},
                    {field:"roleList",title:"拥有角色",width:200,align:"center",
                        formatter: function(value,row,index){
                            if (value != null){
                                var content = '<select>';
                                $.each(value,function (index,val) {
                                    content += '<option>'+val.rolename+'</option>';
                                });
                                content += '</select>';
                                return content;
                            } else {
                                return null;
                            }
                        }
                    }
                ]]
            });
        });
        $(function () {
            $("#cc").combotree({
                checkbox:true,
                width:100,
                url:"${pageContext.request.contextPath}/role/queryAllRoleForAdmins",
                required:true,
                multiple:true
            });
        });

        function updataUser() {
            var admins = $("#showAllcmfz_user").datagrid("getSelected");
            var id = admins.id;
            var name = admins.username;
            var password = admins.beforepassword;
            var roleList = admins.roleList;
            $("#upId").prop("value",id);
            $("#upUserName").prop("value",name);
            $("#upPassWord").prop("value",password);
            $.each(roleList,function (index,value) {
                var t = $("#cc").combotree("tree");
                var node = t.tree("find",value.id);
                t.tree("check",node.target);
            });
            $("#updateUser").dialog('open');
        }
        function upUser() {
            var id = $("#upId").prop("value");
            //alert(id);
            var username = $("#upUserName").prop("value");
            //alert(username);
            var password = $("#upPassWord").prop("value");
            //alert(password);
            var nodes = $("#cc").combotree("getValues");
            //alert(nodes);
            $.ajax({
                url:"${pageContext.request.contextPath}/admins/updateAdmins",
                //async:true,
                traditional:true,
                data:"id="+id+"&username="+username+"&beforepassword="+password+"&ids="+nodes,
                dataType:"json",
                type:"post",
                success:function(map){
                    //清空表单数据
                    $("#updateUserInformation").form('clear');
                    //关闭添加表格的会话框
                    $("#updateUser").dialog('close');
                    $('#showAllcmfz_user').datagrid('reload');
                }
            });
        }
	</script>
</head>
<body>
<table id="showAllcmfz_user">
	<div id="lele">
		<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-add'" onClick="add()">添加</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-edit'" onClick="updataUser();">修改</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-remove'">删除</a>
	</div>
</table>
<div id="addUser" style="display:none">
	<form id="addUserInformation"  method="post" style="margin-left: 50px">
		<br>
		用户名：&nbsp;<input name="username"><br>
		用户密码：&nbsp;<input name="password"><br>
		角色分配：&nbsp;<input name="password"><br>
		<br>
		<br>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="easyui-linkbutton" data-options="plain:true,width:60,iconCls:'icon-add'" >确定</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="easyui-linkbutton" data-options="plain:true,width:60,iconCls:'icon-remove'" >取消</a>
	</form>
</div>
<div id="updateUser" style="display: block">
	<form id="updateUserInformation" enctype="multipart/form-data" method="post">
		<br>
		用户名：&nbsp;&nbsp;&nbsp;<input name="username" id="upUserName" readOnly><br>
		<input name="id" id="upId" hidden="hidden">
		<input name="passwordSalt" id="upPasswordSalt" hidden="hidden">
		用户密码：&nbsp;<input name="beforepassword" id="upPassWord"><br>
		角色分配：&nbsp;<select name="" id="cc"></select>
		<br>
		<br>
		<div id="lalala">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="easyui-linkbutton" data-options="plain:true,width:60,iconCls:'icon-add'" onClick="upUser();">确定</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="easyui-linkbutton" data-options="plain:true,width:60,iconCls:'icon-remove'" onClick="quitAdmin();">取消</a>
		</div>
	</form>
</div>
<div>

</div>
<script type="text/javascript">
    $("#updateUser").dialog({
        title: '修改用户角色',
        closed: true,
        onClose:function(){
            // 在面板关闭后,初始化tree控件
            $("#cc").combotree("clear");
        }
    });
</script>
</body>
</html>
