<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'cmfz_listen.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
      <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
      <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
      <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">
      <script type="text/javascript">
          $(function () {
              $("#showAllcmfz_user").datagrid({
                  url: "${pageContext.request.contextPath}/userpoi/",
                  title: '上师管理',
                  remoteSort: false,
                  nowrap: false,
                  pagination: true,
                  pageSize: 5,
                  pageList: [5, 10, 20, 50],
                  fitColumns: true,
                  toolbar: "#btn",
                  method: "get",
                  columns: [[
                      {field: 'ck', title: '选择', checkbox: true},
                      {field: 'userId', title: '用户 ID', width: 80},
                      {field: 'name', title: '用户名字', width: 100, sortable: true},
                      {field: 'nickname', title: 'List Price', width: 80, align: 'right', sortable: true},
                      {field: 'userImage', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'sex', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'autograph', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'telphone', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'userSheng', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                      {field: 'userShi', title: 'Unit Cost', width: 80, align: 'right', sortable: true},
                  ]],

              });
          })

          function add() {
              $("#addguru").dialog('open')
          }

          function addguru() {
              $("#addguruInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/userpoi/adduserpoi",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#addguru').dialog('close');
                      $('#addguruInformation').form('clear');
                      $('#showAllcmfz_user').datagrid('reload');
                  }
              })
          }

          function removeBatch() {
              var ps = $("#showAllcmfz_user").datagrid('getChecked');
              var aid = "";
              $.each(ps, function (i, p) {
                  aid += "&aid=" + p.siId;
              });
              aid = aid.substring(1);
              console.log(aid);
              $.ajax({
                  url: "${pageContext.request.contextPath}/userpoi/removeBatch?"+aid,
                  dataType: "json",
                  method: "get",
                  success: function (obj) {
                      $('#showAllcmfz_user').datagrid('reload');

                  }

              });
          }

          function update() {
              var ps=$("#showAllcmfz_user").datagrid('getChecked');
              var sid="";
              $.each(ps,function (i,p) {
                  sid = p.siId
              });

              console.log(sid);
              $("#updateguruInformation").form('load',"${pageContext.request.contextPath}/userpoi/selectOne?sid="+sid)
              $("#updateguru").dialog('open')
          }
          function updateguru() {
              $("#updateguruInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/userpoi/Update",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#updateguru').dialog('close');
                      $('#updateguruInformation').form('clear');
                      $('#showAllcmfz_user').datagrid('reload');
                  }
              })
          }



          function exportUser(){
              var ids=$("#showAllcmfz_user").datagrid("getChecked");
              //定义变量用于拼接id字符串
              alert(ids)
              var id="";
              //循环拼接id
              $.each(ids,function(i,n){
                  id=id+"&id="+n.userId;
              });
              alert(id)
              //由于id拼接后第一个多一个&，所以应该去除第一个字符，求该下标后面的子串
              id=id.substring(1);
              window.location.href="${pageContext.request.contextPath}/userpoi/exportUser?"+id;
          }
          /*导入用户*/
          function importUser(){
              $("#queryUserForm").form("submit",{
                  url:"${pageContext.request.contextPath}/userpoi/importPoi",
                  success:function(rest){
                      $("#showAllcmfz_user").datagrid("reload");
                      var str=JSON.parse(rest);
                      if("200"==str.code){
                          $.messager.show({
                              title:'提示信息',
                              msg:str.msg,

                          });
                      }else{
                          $.messager.show({
                              title:'提示信息',
                              msg:str.msg
                          });
                      }
                  }
              });
          }

      </script>
  </head>
  <body>
  <!-- form表单不能放置，table内部，会导致表单无效 -->
  <form id="queryUserForm"  method="post" enctype="multipart/form-data">
    <table id="showAllcmfz_user">
    		<div id="lele"> 
               <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-add'" onClick="add()">添加</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-edit'">修改</a>
                 <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" data-options="iconCls:'icon-remove'">删除</a>
		           <a href="javascript:void(0)" class="easyui-linkbutton" plain="true"  data-options="iconCls:'icon-print'" onClick="exportUser();">导出</a>
		           <input  id="fileName" class="easyui-filebox" name="file"  style="width:200px;vertical-align:middle;" />
					<a href="javascript:void(0)" onclick="importUser()" class="easyui-linkbutton">开始导入</a>
            </div> 
    </table>
    </form>
  <div id="add_User"  data-options="closed:true" class="easyui-dialog">
      <form id="addf" enctype="multipart/form-data" method="post">
          <input  name="userId" style="display: none">
          用户名字：<input  name="name"><br>
          用户昵称：<input  name="nickname" ><br>
          用户性别：<input  name="sex" ><br>
          用户头像 <input name="uploadFile" class="easyui-filebox" data-options="
  			buttonText:'选择文件'
  		"><br>
          用户签名：<input name="autograph"  type="text" ></input><br>
          用户电话：<input name="telphone" type="text" ></input><br>
          用户所在省 <input name="userSheng"  type="text" ></input> <br>
          用户所在市 <input name="userShi" type="text"></input><br>

      </form>
      <center>
          <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="confirmAdd()">确认</a>
          <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
      </center>
  </div>
  <div id="update_User"  data-options="closed:true" class="easyui-dialog">
      <form id="updateform" enctype="multipart/form-data" method="post">
          <input id="us_id" name="userId" style="display: none">
          用户名字：<input id="us_name" name="name"><br>
          用户昵称：<input id="us_niName" name="nickname" ><br>
          用户性别：<input id="us_sex" name="sex"><br>
          用户头像 <input name="uploadFile" class="easyui-filebox" data-options="
  			buttonText:'选择文件'
  		"><br>
          用户签名：<input name="autograph" id="us_Card" type="text"  ></input><br>
          用户电话：<input name="telphone" id="us_tel" type="text" ></input><br>
          用户所在省 <input name="userSheng" id="us_province" type="text" ></input><br>
          用户所在市 <input name="userShi" id="us_city" type="text" ></input><br>

      </form>
      <center>
          <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="confirmUpdate()">确认</a>
          <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
      </center>
      <p id="Article_picture"></p>
  </div>
  
	<div>
	
	</div>
  <script type="text/javascript">
    
  </script>
	 </body>
</html>
