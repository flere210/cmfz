<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'cmfz_listen.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">


  </head>
  <body>
  <input id="cc" class="easyui-combotree">

    <table id="showAllcmfz_role">

	</table>
    <div id="toolbar">
  		
    	<a href="javascript:void(0)" class="easyui-linkbutton" onClick="addRole();" data-options="iconCls:'icon-add'"></a>
    	
    	<a href="javascript:void(0)" class="easyui-linkbutton"  onClick="uRole();" data-options="iconCls:'icon-edit'"></a>
   		
   		<a href="javascript:void(0)" class="easyui-linkbutton"  onClick="removeRole();" data-options="iconCls:'icon-remove'"></a>
   		
    </div>
    
    <div id="addRole" class="easyui-dialog" data-options="closed:true">
    	<form id="addRoleform" method="post" >
    		<%--<ul id="t1" class="easyui-tree"></ul>--%>
			<input id="t1" name="ids">
    		角色名字：<input name="roleName" type="text" class="easyui-validatebox" data-options="required:true">
    		<br/>

    		<br/>
    		<div align="center">
    		<input onClick="confirmRole();" class="easyui-linkbutton" value="确定" data-options="width:50"/>
    		<input class="easyui-linkbutton" onClick="addQuit();" value="取消" data-options="width:50"/>
    		</div>
    	</form>
    </div>
        <div id="updateRole" style="display: none">
    	<form id="updateRoleform" method="post">
    		<ul id="t2" class="easyui-tree" ></ul>
			<input  id="updateids">
    		<input name="updateid" id="roleid" hidden=hidden">
    		<input name="oldRoleName" id="oldRoleName" hidden=hidden"/>
    		角色名字：<input id="roleName" name="roleName" type="text" class="easyui-validatebox" data-options="required:true">
    		<div align="center">
    		<input onClick="upRole();" class="easyui-linkbutton" value="确定" data-options="width:50"/>
    		<input onClick="quitRole();" class="easyui-linkbutton" value="取消" data-options="width:50"/>
    		</div>
    	</form>
    </div>
	<script type="text/javascript">

        $(function () {
            $("#showAllcmfz_role").datagrid({
                url:"${pageContext.request.contextPath}/Resource/role",
                pagination:true,
                pageSize:3,
                pageList:[3,6,9],
                toolbar:"#toolbar",
                columns:[[
                    {field:"",title:"",checkbox:true},
                    {field:"id",title:"图片ID",width:50,align:"center",hidden:true},
                    {field:"rolename",title:"角色名字",width:150,align:"center"},
                    {field:"list",title:"角色资源",width:200,align:"center",
                        formatter: function(value,row,index){
                            if (value != null){
                                var content = '<select>';
                                $.each(value,function (index,val) {
                                    content += '<option>'+val.text+'</option>';
                                    $.each(val.children,function (index,val1) {
                                        content += '<option>&nbsp;&nbsp;'+val1.text+'</option>';
                                        $.each(val1.children,function (index,val2) {
                                            content += '<option>&nbsp;&nbsp;&nbsp;&nbsp;'+val2.text+'</option>';
                                        })
                                    })
                                });
                                content += '</select>';
                                return content;
                            } else {
                                return null;
                            }
                        }
                    }
                ]]
            });
            // 初始化tree
            $("#t2").tree({
                url: "${pageContext.request.contextPath}/Resource/tree",
                checkbox:true,
                loadFilter: function(data){
                    return data;
                }
            });
        })

        function uRole(){
            $("#updateRole").show();
            $("#updateRole").dialog({
                title:"修改角色",
                onClose:function(){

                    // 在面板关闭后,初始化tree控件
                    $("#t2").tree("reload");

                }
            });

            //
            var roles = $("#showAllcmfz_role").datagrid("getSelected");

            $("#oldRoleName").prop("value",roles.rolename);
            $("#RoleName").prop("value",roles.rolename);
            $("#roleid").prop("value",roles.id);


            var roleList = 	roles.list;
            console.log(roleList);
            $.each(roleList,function (index,val1) {
                // 判断是否为叶子节点
                if(val1.children){
                    console.log("======")
                    $.each(val1.children,function (indx,val2) {
                        if(val2.children.length>0){
                            $.each(val2.children,function (inx,val3) {
                                var node = $("#t2").tree("find",val3.id);
                                $("#t2").tree("check",node.target);
                            })
                        }else{
                            var node = $("#t2").tree("find",val2.id);
                            $("#t2").tree("check",node.target);
                        }

                    })
                }
            })
        }

		function addRole() {
            $("#t1").combotree({
                url: "${pageContext.request.contextPath}/Resource/tree",
                multiple:true,
                loadFilter: function(data){
                    return data;
                }
			})
            $("#addRole").dialog('open');
        }
		
        function confirmRole() {
            $("#addRoleform").form('submit', {
                url: "${pageContext.request.contextPath}/Resource/addRole",
                onSubmit: function () {
                    return true;
                },
                success: function (str) {
                    $('#addRole').dialog('close');
                    $('#addRoleform').form('clear');
                    $('#showAllcmfz_role').datagrid('reload');
                }
            })
        }
		function upRole() {
            var arr = new Array();
            var nodes1 = $("#t2").tree("getChecked");
            var nodes2 = $("#t2").tree("getChecked","indeterminate");

            $.each(nodes1,function (index,value) {
                arr.push(value.id);
            });
            $.each(nodes2,function (index,value) {
                arr.push(value.id);
            });
            $("#updateids").prop("value",arr);
            $("#updateRoleform").form('submit', {
                url: "${pageContext.request.contextPath}/Resource/updateRole?updateids="+arr,
                onSubmit: function () {
                    return true;
                },
                success: function (str) {
                    $('#updateRole').dialog('close');
                    $('#updateRoleform').form('clear');
                    $('#showAllcmfz_role').datagrid('reload');
                }
            })
        }
	</script>
	 </body>
</html>
