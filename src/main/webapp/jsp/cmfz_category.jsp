<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'cmfz_category.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
	  <script type="text/javascript" src="${pageContext.request.contextPath}/js/datagrid-detailview.js"></script>
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/easyui.css">
	  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/icon.css">
	  <script type="text/javascript">
          $(function () {
              $("#showAllcmfz_category").datagrid({
                  url: "${pageContext.request.contextPath}/category/",
                  title: '功课管理',
                  remoteSort: false,
                  nowrap: false,
                  pagination: true,
                  pageSize: 5,
                  pageList: [5, 10, 20, 50],
                  fitColumns: true,
                  toolbar: "#btn",
                  method: "get",
                  columns: [[
                      {field: 'ck', title: '选择', checkbox: true},
                      {field: 'xiuId', title: 'Item ID', width: 80},
                      {field: 'xiuName', title: 'Product ID', width: 100, sortable: true},
                      {field: 'userId', title: 'List Price', width: 80, align: 'right', sortable: true},
                  ]],

              });
          })

          function add() {
              $("#addCategory").dialog('open')
          }

          function addcategory() {
              $("#addCategoryInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/category/addcategory",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#addCategory').dialog('close');
                      $('#addCategoryInformation').form('clear');
                      $('#showAllcmfz_category').datagrid('reload');
                  }
              })
          }

          function removeBatch() {
              var ps = $("#showAllcmfz_category").datagrid('getChecked');
              var xid = "";
              $.each(ps, function (i, p) {
                  xid += "&xid=" + p.xiuId;
              });
              xid = wid.substring(1);
              console.log(xid);
              $.ajax({
                  url: "${pageContext.request.contextPath}/category/removeBatch?"+xid,
                  dataType: "json",
                  method: "get",
                  success: function (obj) {
                      $('#showAllcmfz_category').datagrid('reload');

                  }

              });
          }

          function update() {
              var ps=$("#showAllcmfz_category").datagrid('getChecked');
              var xid = "";
              $.each(ps,function (i,p) {
                  xid = p.xiuId;
              });

              console.log(xid);
              $("#updateCategoryInformation").form('load',"${pageContext.request.contextPath}/category/selectOne?xid="+xid)
              $("#updateCategory").dialog('open')
          }


          function updatecategory() {
              $("#updateCategoryInformation").form('submit', {
                  url: "${pageContext.request.contextPath}/category/Update",
                  onSubmit: function () {
                      return true;
                  },
                  success: function (str) {

                      $('#updateCategory').dialog('close');
                      $('#updateCategoryInformation').form('clear');
                      $('#showAllcmfz_category').datagrid('reload');
                  }
              })
          }
	  </script>

  </head>
  
  <body>
  <div id="btn">
	  <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'"
		 onclick="removeBatch()">批量删除</a>
	  <a href="javascript:void(o)" class="easyui-linkbutton" onclick="add()" data-options="iconCls:'icon-add'">添加</a>
	  <a href="javascript:void(o)" class="easyui-linkbutton" onclick="update()" data-options="iconCls:'icon-add'">修改</a>
  </div>
    <table id="showAllcmfz_category"></table>
    <div id="addCategory" class="easyui-dialog" data-options="closed:true">
    	<form id="addCategoryInformation" enctype="multipart/form-data" method="post">
    		<br>
    		功课名：&nbsp;<input name="category_name"><br>
    		是否必修：<input class="easyui-combobox" name="category_isnecessary" data-options="
    			valueField: 'label',
				textField: 'value',
	    		data: [{
					label: 'Y',
					value: '必修'
				},{
					label: 'N',
					value: '自定义'
				}]
			" />
			<center>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="addcategory()">确认</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
			</center>
    	</form>
    </div>
    <div id="updateCategory" class="easyui-dialog" data-options="closed:true">
    	<form id="updateCategoryInformation" enctype="multipart/form-data" method="post">
    		<br>
    				<input id="category_id" name="category_id" style="display: none">
    		功课名：&nbsp;<input id="category_name" name=category_name><br>
    		是否必修：<input id="category_isnecessary" class="easyui-combobox" name="category_isnecessary" data-options="
	    		valueField: 'label',
				textField: 'value',
	    		data: [{
					label: 'Y',
					value: '必修'
				},{
					label: 'N',
					value: '自定义'
				}]
			" />
			<center>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onClick="updatecategory()">确认</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
			</center>
    	</form>
    </div>
		<script type="text/javascript">

		</script>
  </body>
</html>
